 * -------------------------------------------------------------------------------------------------
 * -------------------------------------------------------------------------------------------------
 * Stress, temperature and peripherals test
 * -------------------------------------------------------------------------------------------------
 * -------------------------------------------------------------------------------------------------
 * @version
 * 0.2 | mc | 06.08.2012
 *
 * @author
 * mc : Miguel Mendez, Seven Solutions SL
 * -------------------------------------------------------------------------------------------------

The following sections describe how the folder and files are organized and what they include:

	StressTestMBP_SCB (Project folder). It includes the compressed EDK project, all files and specific IP cores implemented for this design, so it can be used and modified for the user whenever it is necessary. This project needs the Xilinx tool packet to be opened: EDK tool for HW modifications and SDK tool for software modifications (it is possible to not modify HW (�system.bit� file) and work just with SDK to change the software part).

	Test file (Test folder). It includes the .BIT file ready to be dumped into the FPGA. All HW and SW are introduced inside this file so the project will start sending data through the USB-UART (Minibackplane FPGA-USB connection) as soon as the FPGA is programmed.

	Doc (Document folder). It includes the documents related to this design.

These folders are located in: /hdl/StressTest/
