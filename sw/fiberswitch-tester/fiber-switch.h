#ifndef __FIBER_SWITCH_H__
#define __FIBER_SWITCH_H__

#define FTDI_TXD 0x01
#define FTDI_RXD 0x02
#define FTDI_RTS 0x04
#define FTDI_CTS 0x08
#define FTDI_DTR 0x10
#define FTDI_DSR 0x20
#define FTDI_DCD 0x40
#define FTDI_RI  0x80

#define SW1_OUT1 FTDI_DCD //white
#define SW1_OUT2 FTDI_DSR //purple

#define SW2_OUT1 FTDI_CTS //white
#define SW2_OUT2 FTDI_DTR //purple

#define SW3_OUT1 FTDI_RTS //white
#define SW3_OUT2 FTDI_RXD //purple

#endif
