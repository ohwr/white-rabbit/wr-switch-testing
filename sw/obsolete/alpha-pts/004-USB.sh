#!/bin/ash
## Check the USB connexion
## 
##	This should use USB gadget and the device that need to communicate 
##	are /dev/ttyACM0 on host and /dev/ttyGS0 on switch.
##
##
## Authors: 
##	- Benoit Rat (Seven Solutions, www.sevensols.com)
##
## GNU Lesser General Public License Usage
## This file may be used under the terms of the GNU Lesser
## General Public License version 2.1 as published by the Free Software
## Foundation and appearing in the file LICENSE.LGPL included in the
## packaging of this file.  Please review the following information to
## ensure the GNU Lesser General Public License version 2.1 requirements
## will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
#######################################################################################################

printf "======: $0\n"
. 000-functions.sh

printf "To be implemented\n"
exit_multierr 0

