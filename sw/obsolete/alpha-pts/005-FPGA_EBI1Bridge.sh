#!/bin/sh
## Testing the cpu-fpga bus (EBI1)
## 
## 
## Using devmem we read/write in the memory range 0x10000000-0x10004000 (16KiB)
## The bridge used can be find here: http://www.ohwr.org/projects/general-cores/repository/revisions/master/show/modules/wishbone/wb_async_bridge
## And it is defined using PC16..31: periphA as EBI1_D16..31
##
## Authors: 
##	- Benoit Rat (Seven Solutions, www.sevensols.com)
##
## GNU Lesser General Public License Usage
## This file may be used under the terms of the GNU Lesser
## General Public License version 2.1 as published by the Free Software
## Foundation and appearing in the file LICENSE.LGPL included in the
## packaging of this file.  Please review the following information to
## ensure the GNU Lesser General Public License version 2.1 requirements
## will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
#######################################################################################################

printf "======: $0\n"
. 000-functions.sh

#First loading the correct firmwares:
load-virtex ${APTS_HDL_18P}
load-lm32 ${APTS_FW_RTCPU}

nTot=512
addr=0x10000000
end=0x10004000
step=32

nOK=0

let addr10=$addr
let end10=$end


printf "Testing CPU-FPGA bus (EBI0 @  $addr > $end): "
while [ $addr10 -lt $end10 ]; do


	rand=`dd if=/dev/urandom count=1 2> /dev/null | cksum | cut -f1 -d" "`
	val=`printf "0x%08X" $rand`

	devmem $addr 32 $val
	ret=`devmem $addr`

	if [ $val != $ret ]; then
		echo "ERROR @ $addr W:$val, R:$ret"
	else
		nOK=`expr ${nOK} + 1` 
	fi         


	if [ `expr $addr10 % 1024`  -eq 0 ]; then
    		printf "."
	fi


	addr10=`expr $addr10 + $step`
	addr=`printf "0x%X" $addr10`

done

echo ""
echo "Result CPU-FPGA bus: OK=${nOK}/${nTot}W"
exit_errpercent ${nOK} ${nTot}
