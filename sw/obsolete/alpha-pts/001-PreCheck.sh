#!/bin/sh
## Read the MD5 of the different binaries used by the test
##
##	This is intended to know which version of the test and its files we are using
##
##
## Authors: 
##	- Benoit Rat (Seven Solutions, www.sevensols.com)
##
## GNU Lesser General Public License Usage
## This file may be used under the terms of the GNU Lesser
## General Public License version 2.1 as published by the Free Software
## Foundation and appearing in the file LICENSE.LGPL included in the
## packaging of this file.  Please review the following information to
## ensure the GNU Lesser General Public License version 2.1 requirements
## will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
#######################################################################################################

printf "======: $0\n"
. 000-functions.sh

uname -a
ret0=$?
printf "MAC="$(ifconfig | grep 'eth0'    | tr -s ' '    | cut -d ' ' -f5)"\n" 
ret1=$?

ver="$(shw_ver -a)"
ret2=$?
printf "shw_ver=%s\n" "$ver"

printf "printing md5sum the loading file:\n"
md5sum "$(which load-virtex)"
ret3=$?
md5sum "$(which load-lm32)"
ret4=$?
md5sum "$(which shw_ver)"
ret5=$?
md5sum "$(which shw_tool)"
ret6=$?
md5sum "$(which wrs-iftool)"
ret7=$?


md5sum ${APTS_HDL_18P}
ret8=$?
md5sum ${APTS_FW_RTCPU}
ret9=$?

printf "printing md5sum of the binaries:\n"
md5sum bin/*
ret10=$?



exit_multierr $ret0 $ret1 $ret2 $ret3 $ret4 $ret5 $ret6 $ret7 $ret8 $ret9 $ret10

