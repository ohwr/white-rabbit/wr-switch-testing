#!/bin/ash

# setupGlobalVar exportname value
setupGlobalVar() 
{
	export | grep $1 &> /dev/null
	if [ $? -gt "0" ]; then
		export $1="$2"
		echo $1=$2
	else
		echo "Already set"
	fi
}

# checkLoadTFTP filename updateflag
checkLoadTFTP() 
{
	fname=$1
	upflag=$2
	dest=$3
	_ret=1
	

	# Check destination directory
	if [[ -z $dest ]]; then
		dest=${APTS_HOST_IP}/bin/
	fi
	oldir=$(pwd)
	cd ${dest}
	
	
	if [[ -f $fname ]]; then
		upflag="1"
	fi
	
	if [[ $upflag ]]; then
		tftp -g -r ${APTS_HOST_DIR}/$fname ${APTS_HOST_IP}
		
		_ret=$?
		ret=$(( $ret + ${_ret} ))
		if [[ $ret != "0" ]]; then
			printf "ERROR: While downloading $fname\n"
		fi
	fi
	
	cd ${oldir}
	return ${_ret}
}

#Create a sound with the FAN to enter value on keyboard
alarm()
{
	for itest in 01 02 03; do
	
		shw_tool --fan 100
		sleep 1	
		shw_tool --fan 0
		sleep 1
		echo "$itest"
	done
}

load_ask2run() 
{
	sh=$1
	ask=$2
	update=$3
	inlog=$4
	valid="Y"

	itest=$( echo $sh | sed 's/\([0-9]*\)-.*/\1/' )

	if [ ${ask} = "1" ]; then
		read -t 5 -p "# Execute $sh? (timeout in 5s) [Y/n/c]: " valid;
	fi

	if [ "${valid}" == "c" ]; then
		echo -e "Cancel the test"		
		exit 0;
	elif [ "${valid}" == "n" ]; then
		echo -e "Skip the script $sh"
	else
		echo ""
		ts=$(date +%s)
		# Load script 
		chmod +x ${APTS_SWITCH_DIR}/$sh
		
		printf "\n\n" > ${logfname}_${itest}.log

		# Run it into the log
		if [ $inlog -eq "1" ]; then
			echo "inlog"
			${APTS_SWITCH_DIR}/$sh $update ${logfname}_${itest}.log
		else
			${APTS_SWITCH_DIR}/$sh $update 2>&1 | tee -a ${logfname}_${itest}.log
		fi
		ret=$(cat /tmp/func_ret)
		
		ts=$(( $(date +%s) - $ts))
		min=$(($ts / 60 ))
		sec=$(($ts % 60 ))
		
		printf "Running time: %d'%d\"\n\n"  $min $sec
		printf "\t%s > %s (%02d'%02d)\n" ${itest} "${ret}" ${min} ${sec}  >> ${logfname}.log
	fi
}


waittime() 
{
	timeout=$1
	
	i=0
	while [ $i -lt $timeout ]; do
		printf "."
		sleep 1
		i=`expr $i + 1`
	done
	echo ""
}
	

#user_echoasking type question tagname
#output
#	ans= is the answer selected
# return value is 0 except when we reply no.
user_echoasking() 
{
	type="$1"
	question="$2"
	tagname="$3"
	ans="";
	ret="0"	
	loop="1"
	while [ $loop == "1" ]; do
		read -p "$question [$type]: " ans
		case ${ans} in
			y|Y)	if [ $type != "num" ]; then ans="y"; loop="0"; fi;;
			n|N) 	if [ $type != "num" ]; then ans="n"; ret="1"; loop="0"; fi;;
			[0-1]*) if [ $type == "bin" -o $type == "num" ]; then loop=0; fi;;
			[0-9]*) if [ $type == "num" ]; then loop=0; fi;;
			*) if [ $type == "Y/n" ]; then 
				ans="y"
				loop=0 
			elif  [ $type == "y/N" ]; then
				ans="n"
				ret="1"
				loop=0
			fi;;
		esac
	done
	echo "$tagname=$ans"
	return $ret;
}

errpercent() 
{
	if [ $# -eq "0" ]; then
		return 0
	elif [ $# -eq "1" ]; then
		return $1
	else
		nOK=$1
		nTotal=$2
		if [ ${nOK} -eq ${nTotal} ]; then
			return  0
		else
			##Return the %of fault
			tmp=$(($(($((${nTotal} - ${nOK})) \*100 )) / ${nTotal} ))
			if [ $tmp -eq "0" ]; then
				tmp=1; ##Transform 0.x% to 1%
			fi
			return $tmp
		fi
	fi
}

exit_errpercent() 
{
	errpercent $1 $2
	ret=$?
	if [ $ret -eq "0" ]; then
			ret="OK"
		else
			ret="ERROR $ret%"
		fi  
	echo "$ret" > /tmp/func_ret
	exit 0	
	
}


exit_multierr()
{
	retmsg=""
	oneERR="0"
	for i in $(seq 1 $# ); do
		ret=$(eval echo "\$${i}")
		if [ $ret -eq "0" ]; then
				ret="OK"
		else
			oneERR="1"
			if [ $ret -lt "0" ]; then
				ret="$ret (100%)"
			else
				ret="$ret%"
			fi
		fi 
		retmsg="$retmsg Sub$(($i-1)):$ret"	 
	done
	
	if [ $oneERR -ne "0" ]; then
		retmsg="ERROR ($retmsg)"
	else
		retmsg="OK"
	fi
	echo "$retmsg" > /tmp/func_ret
	exit 0	
}
