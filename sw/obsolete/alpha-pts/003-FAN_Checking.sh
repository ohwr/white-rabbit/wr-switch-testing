#!/bin/ash
## FAN checking
## 
##	Test the different FANs of the switch at different speed before starting the temperature monitoring.  
##
##
## Authors: 
##	- Benoit Rat (Seven Solutions, www.sevensols.com)
##
## GNU Lesser General Public License Usage
## This file may be used under the terms of the GNU Lesser
## General Public License version 2.1 as published by the Free Software
## Foundation and appearing in the file LICENSE.LGPL included in the
## packaging of this file.  Please review the following information to
## ensure the GNU Lesser General Public License version 2.1 requirements
## will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
#######################################################################################################

printf "======: $0\n"
. 000-functions.sh

#First load the correct firmwares:
load-virtex ${APTS_HDL_18P}
load-lm32 ${APTS_FW_RTCPU}

speed=100
while [ ${speed} -gt 30 ]; do
	echo "FAN1 @ $speed %"
	shw_tool --fan -m 1 ${speed}
	sleep 1
	speed=$(expr $speed - 10)
done
shw_tool --fan -m 1 0
ans=""
user_echoasking "y/n" "Did you hear the speed of FAN 1 decrease" "fan_speed_up"
ret0=-$?

speed=100
while [ ${speed} -gt 30 ]; do
	echo "FAN2 @ $speed %"
	shw_tool --fan -m 2 ${speed}
	sleep 1
	speed=$(expr $speed - 10)
done
shw_tool --fan -m 2 0
ans=""
user_echoasking "y/n" "Did you hear the speed of FAN 2 decrease" "fan_speed_up"
ret0=-$?

shw_tool --fan 80
if [ $ans == "n" ]; then
	user_echoasking "y/n" "Does one of the FAN is working?" "fan_enable"
	ret1=-$?
	if [ $ans == "n" ]; then
		echo "WARNING: NO FAN DETECTED! YOU SHOULD STOP THIS TEST BEFORE OVERHEATING YOUR FPGA"
	fi
fi

exit_multierr $ret0 $ret1



