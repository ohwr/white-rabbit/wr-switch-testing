% Alpha PTS scripts
% Benoit RAT, benoit@sevensols.com 
% July 2012

Binaries
============


* **fpga_scbtest.bit** &  **cpu_rt.bin** correspond to a fix version of the
switch HDL (`bfbeadaaa3cdbc73e38c20dcc06901262fb9bad4`).
They are going to be removed when a fixed HDL version is going
to be release.

* **fpga_qdr2temp.bit** is the bitstream to check & stress the QDRII while
monitoring the temperatures and using UART to display their values. 
For the moment, this bitstream is completely isolated from
the ARM. The corresponding source code can be found in
`/hdl/qdr2temp/` folder
**CAUTIONS**: This bitstream is designed to increase the temperature, 
therefore it can be armful if it is not used with a FAN.


--------------------------------------------------------
*This document is written in 
[markdown syntax](http://johnmacfarlane.net/pandoc/README.html#pandocs-markdown), 
and can be compiled by executing `pandoc --toc --number-section README.mkd -o README.pdf`*
