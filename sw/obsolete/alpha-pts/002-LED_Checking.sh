#!/bin/ash
## LED checking
## 
##	Load the standard bitstream to active the GPIOs of the LEDs. 
##	then it use HAL to run a LED moving pattern.  
##
##
## Authors: 
##	- Benoit Rat (Seven Solutions, www.sevensols.com)
##
## GNU Lesser General Public License Usage
## This file may be used under the terms of the GNU Lesser
## General Public License version 2.1 as published by the Free Software
## Foundation and appearing in the file LICENSE.LGPL included in the
## packaging of this file.  Please review the following information to
## ensure the GNU Lesser General Public License version 2.1 requirements
## will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
#######################################################################################################

printf "======: $0\n"
. 000-functions.sh

#First loading the correct firmwares:
load-virtex ${APTS_HDL_18P}
load-lm32 ${APTS_FW_RTCPU}

ret0=0
ret1=0
ret2=0
ret3=0
retstr=""


led_pattern="green orange yellow none"

iled_id=0
for nled in ${led_pattern}; do
	shw_tool --led -i ${iled_id}
	iled_id=$(($iled_id + 1 ))
	
	user_echoasking "y/N" "Did you see the LEDs in ${nled}" "LEDS_${nled}"
	retstr="$retstr -$?"
	if [ $ans == "n" ]; then
		read -p "Write the failing LED for color : " ans
		echo "$ans" 
	fi
	
done


exit_multierr $retstr

