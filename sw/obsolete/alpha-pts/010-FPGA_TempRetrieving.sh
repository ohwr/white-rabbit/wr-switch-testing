#!/bin/sh
## Retrieve Temperatures
##
##	Read the different temperature sensor at the end of the test
##
## Authors: 
##	- Benoit Rat (Seven Solutions, www.sevensols.com)
##
## GNU Lesser General Public License Usage
## This file may be used under the terms of the GNU Lesser
## General Public License version 2.1 as published by the Free Software
## Foundation and appearing in the file LICENSE.LGPL included in the
## packaging of this file.  Please review the following information to
## ensure the GNU Lesser General Public License version 2.1 requirements
## will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
#######################################################################################################

printf "======: $0\n"
. 000-functions.sh

#First loading the correct firmwares:
load-virtex ${APTS_HDL_18P}
load-lm32 ${APTS_FW_RTCPU}

alarm;
shw_tool --fan 80

echo "Display 3 times the temperatures"

shw_tool --mon -l 3

user_echoasking "y/n" "Do you see all temperature message on UART?" "UART temp"
ret0=-$?
if [ $ans == "n" ]; then
	read -p "Write the name of the sensors that doesn't work: " ans
	echo "$ans" 
fi

exit_multierr $ret0



