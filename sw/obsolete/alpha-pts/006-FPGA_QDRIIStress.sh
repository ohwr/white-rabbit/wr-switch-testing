#!/bin/sh
## Testing QDRII, PLL, and GPIOs
##
##	This test stress the QDRII at maximum, and start monitoring the 
##	different temperature sensor.
##	
##
##	Future Improvements:
##	- There are no communications between the FPGA and CPU
##
##
## Authors: 
##	- Benoit Rat (Seven Solutions, www.sevensols.com)
##
## GNU Lesser General Public License Usage
## This file may be used under the terms of the GNU Lesser
## General Public License version 2.1 as published by the Free Software
## Foundation and appearing in the file LICENSE.LGPL included in the
## packaging of this file.  Please review the following information to
## ensure the GNU Lesser General Public License version 2.1 requirements
## will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
#######################################################################################################

printf "======: $0\n"
. 000-functions.sh

#Check version to load or not the QDRII test
ver=$(shw_ver -p)
maxver="3.3"
res=$(echo | awk -v n1=$ver -v n2=$maxver  '{if (n1<n2) printf("1\n"); else printf("0\n");}')
if [ "x${res}" == "x1" ]; then


	#First loading the correct firmwares:
	load-virtex ${APTS_SWITCH_DIR}/bin/fpga_qdr2temp.bit

	timeout=3 #Timeout before reading
	printf "FPGA QDRII stress and temperature monitoring (${timeout}s): "
	waittime $timeout

	#Setting the fan at 70%
	${APTS_SWITCH_DIR}/bin/shw_tool --fan 60

	user_echoasking "y/n" "Do you see the 3 + 1 last LEDs on the miniBP?" "LED QDRII"
	ret0=-$?
	if [ $ans == "n" ]; then
		user_echoasking "bin" "LED pattern?" "LED patterm QDRII"
	fi;	

	exit_multierr $ret0 

else
	echo "OK: No QDRII mounted for v$ver (>=$maxver)"
	exit_multierr 0
fi