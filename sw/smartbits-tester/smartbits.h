#ifndef __SMARTBITS_H__
#define __SMARTBITS_H__

#define HUB 0
#define SLOT 7
#define IPADR "10.11.34.101"
//#define IPADR "127.0.0.1"

#define FIBER 1
#define BANK  0

//FIBER 0 BANK 0 -> regular ETH switch
//FIBER 1 BANK 0 -> WR switch
//FIBER 0 BANK 1 -> PCBE13137
//FIBER 1 BANK 1 -> loopback

/****** SmartBits device ******/
#define SMB_PORTS 4
#define SMB_P0  0x01
#define SMB_P1  0x02
#define SMB_P2  0x04
#define SMB_P3  0x08
#define SMB_PALL (SMB_P0 | SMB_P1 | SMB_P2 | SMB_P3)

#define PORT_TO_PMASK(i) (1<<(i))

#define MAX_STREAMS 4
#define FRAME_SZ 60
#define LINKUP_TIMEOUT 10
#define TRAFFIC_START 1
#define TRAFFIC_STOP 0

// macro CHECKERROR:
//    in case of error displays the function name and the status
#define CHECKERROR(givenfunction) \
   {\
      int _____ireturncode = (givenfunction); \
      if( _____ireturncode < 0 )  \
         fprintf(stderr, "\n%s = %d\n", #givenfunction, _____ireturncode); \
   }

struct smb_test;

void linkToSmartBits();
void setTrigger(int h1, int s1, int p1);
void clearCounters(int h1, int s1, int p1);
int  set_port_config(int p);
void show_card_info(int p);
void show_cntrs(HTCountStructure *cs, int p1);
void latency_meas(int p_tx, int p_rx);
int  basic_traffic(int fsize, int p_tx, int p_rx);
int setup_stream(int port, char *dmac, char *smac, int fsize, int fnum, float ratio, int v_pri, int vid, int idx);
int temp_run_test(int p_tx, int p_rx);

int detect_stop(void);
long ratio_to_fps(int frame_size, float ratio);
long ratio_to_gap(int frame_size, float ratio);

int get_allcntrs(int mask, HTCountStructure *cntrs);
int show_allcntrs(FILE *f, int mask, HTCountStructure *cntrs);
int clear_allcntrs(int mask);
int show_floss(FILE *f, HTCountStructure *cntrs, unsigned long *total);
int reset_ports(int mask);
int run_traffic(int mask, int start);
int smb_learning(int mask, char [][6], int fnum);
int wait_linkup(int mask);
int smb_set_trigger(char *pat, int port, int offset, int range, int trigno);
int wait_test_done(int mask, int sec);
int exp_rx_frames(struct smb_test *wrst, HTCountStructure *cntrs, unsigned long *exp);

#endif
