#ifndef __TESTS_BODY_H__
#define __TESTS_BODY_H__

#include "et1000.h"
#include "smartbits.h"

/* functional tests */
#define TEST_2_1_1 0
#define TEST_2_1_2 1
#define TEST_2_2_1 2
#define TEST_2_2_2 3
#define TEST_2_2_5 4
#define TEST_2_2_6 5
#define TEST_2_2_7 6
/* performance tests */
#define VAR1 0
#define VAR2 1
#define TEST_3_1_1 7
#define TEST_3_1_2 8
#define TEST_3_1_3 9
#define TEST_3_1_4 10
#define TEST_3_1_5 11
#define TEST_3_2_1 12
#define TEST_3_2_2 13
#define TEST_3_3_1 14
#define TEST_3_3_2 15

extern const char* TLIST[16];

//#define DPORT_NONE 0xFE
#define TX_PALL 0x4
#define VID_NONE -1
#define RX_PALL 0xFF
#define RX_ALLUNI 0xFE

#define VLAN_TAG(vid,prio) ((0x8100 << 16) | (prio<<13) | (vid))
#define NOVLAN_TAG 0x0800

#define TEST_PASSED 2
#define TEST_WARNING 1
#define TEST_FAILED 0

#define NEXT_STEP 1
#define NONEXT_STEP 0

#define TEST_FUNCTIONAL  0
#define TEST_PERFORMANCE 1

/* Tests parameters */
#define T_2XX_FNUM 1000
#define T_2XX_UNRECOG_THR 10

#define FLOAD_MAXTAB 12
#define FSIZE_MAXTAB 6

struct smb_port_tx {
  int d_ports[SMB_PORTS];  //SMB destination port index [0..3]; 0xff for broadcast
  int d_ports_n;
  int q_vid;     //VLAN ID, if 802.1d, make it -1
  int q_pri;    //priority, if 802.1d, make it -1
  int fsize;     //size of transmitted frames
  int fsize_start;
  int fsize_step;
  int fsize_stop;
  int fsize_tab[FSIZE_MAXTAB];
  int fsize_n;
  int fload;      /* currently used frame load */
  int fload_tab[FLOAD_MAXTAB];     //traffic load tab to test[1..100]%
  int fload_n;
  int fnum;      //amount of frames sent in a single burst
};

struct smb_port_rx {
  int  s_port;       //Source SMB port for setting TRIG1
  char trig2_len;    //TRIG2 pattern length, if 0 TRIG2 not set
  char trig2_pat[6]; //TRIG2 pattern - Ethtype + VLAN tag
};

struct smb_test {
  struct smb_port_tx ptx[SMB_PORTS];
  struct smb_port_rx prx[SMB_PORTS];
  int learning;
  int variants;
  int ftime;     //length of burst in seconds
  int test_type;
  int permute;
  int force_prst; // force ports reset for each tested load/size
};

void init_wr_tests(int num, int variant);
int run_test( HTCountStructure *cntrs, char mac[][6] );
int anl_result( HTCountStructure *cntrs, int testno);
int run_and_analyze(int test_no, FILE *logfile);
int next_testcase(int reset);

#endif
