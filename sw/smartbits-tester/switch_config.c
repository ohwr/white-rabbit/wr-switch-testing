#include <stdio.h>
#include <stdlib.h>
#include "switch_config.h"

int wrs_config(char *ip, int testno)
{
  char cmd[100];
  int list[16] = {211, 212, 221, 222, 225, 226, 227, 311, 312, 313, 314, 315, 321, 322, 331, 332};

  sprintf(cmd, "ssh -p %d root@%s %s %d >/dev/null 2>&1", WRS_PORT, ip, WRS_CONF_SCRIPT, list[testno]);
  return system(cmd);
}
