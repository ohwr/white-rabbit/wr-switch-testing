#!/bin/sh

# remove all possible VLANs
/wr/bin/rtu_stat vlan 1 1 0x0 1
/wr/bin/rtu_stat vlan 2 2 0x0 1
/wr/bin/rtu_stat vlan 3 3 0x0 1
/wr/bin/rtu_stat vlan 4 4 0x0 1
/wr/bin/rtu_stat vlan 5 5 0x0 1
/wr/bin/rtu_stat vlan 6 6 0x0 1
/wr/bin/rtu_stat vlan 7 7 0x0 1
/wr/bin/rtu_stat vlan 8 8 0x0 1
/wr/bin/rtu_stat vlan 9 9 0x0 1

/wr/bin/rtu_gd -g $1

if [ "$1" == "211" ] || [ "$1" == "212" ] || [ "$1" == "311" ] || [ "$1" == "312" ] || [ "$1" == "313" ] || [ "$1" == "331" ]; then
  #remove VLAN 1
  /wr/bin/rtu_stat vlan 1 1 0x0 1
fi

if [ "$1" == "221" ] || [ "$1" == "225" ] || [ "$1" == "226" ]; then
  #configure VLAN 1
  /wr/bin/rtu_stat vlan 1 1 0x3
fi

if [ "$1" == "222" ]; then
  /wr/bin/rtu_stat vlan 1 1 0x1
fi

if [ "$1" == "227" ] || [ "$1" == "321" ] || [ "$1" == "322" ]; then
  /wr/bin/rtu_stat vlan 1 1 0x20001
  /wr/bin/rtu_stat vlan 2 2 0x00006
  /wr/bin/rtu_stat vlan 3 3 0x00018
  /wr/bin/rtu_stat vlan 4 4 0x00060
  /wr/bin/rtu_stat vlan 5 5 0x00180
  /wr/bin/rtu_stat vlan 6 6 0x00600
  /wr/bin/rtu_stat vlan 7 7 0x01800
  /wr/bin/rtu_stat vlan 8 8 0x06000
  /wr/bin/rtu_stat vlan 9 9 0x18000
fi

if [ "$1" == "314" ] || [ "$1" == "315" ] || [ "$1" == "332" ]; then
  /wr/bin/rtu_stat vlan 1 1 0x3
  /wr/bin/rtu_stat vlan 2 2 0x30000
fi

