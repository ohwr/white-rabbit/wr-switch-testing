#ifndef __SWITCH_CONFIG_H__
#define __SWITCH_CONFIG_H__

#define WRS_PORT 2223
#define WRS_CONF_SCRIPT "/root/wrs_vlan_config.sh"

int wrs_config(char *ip, int testno);

#endif
