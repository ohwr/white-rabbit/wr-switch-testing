#!/bin/bash

# License: GPL v2 or later.
# Website: http://www.ohwr.org

LOGDIR=./log_wrs

mkdir -p $LOGDIR
sudo rm -fr $LOGDIR/pts*

if [ x$1 = x"" ]; then
    echo -n "Please enter the 2-digit numbers seperated by spaces (00 01 10) of the tests you want to perform , then press [ENTER]: "
        read selection
else
    selection="$@"
fi

if [[ $selection != "00"* ]]; then
    echo -n "You didn't select test00, please make sure testfirmware is loaded"
    read temp
    echo " "
fi

if [[ $selection != *"01"* ]]; then
    echo -n "You didn't select test01, please make sure switch is booted from nfs"
    read temp
    echo " "
fi

echo -n "Please scan CERN serial number bar-code, then press [ENTER]: "
    read serial

if [ x$serial = x"" ]; then
    serial=0000
fi

echo -n "If needed input extra serial number and press [ENTER] OR just press [ENTER]: "
    read extra_serial

if [ x$extra_serial = x"" ]; then
    extra_serial=0000
fi

echo " "

nb_test_limit=2
nb_test=1

while [ "$nb_test" -le "$nb_test_limit" ]
do
    echo "--------------------------------------------------------------"
    echo "Test series run $nb_test out of $nb_test_limit"
    echo " "

    sudo ./pts.py -b WRS -s $serial -e $extra_serial -t./test/wrs/python -l $LOGDIR $selection

    if [ "$nb_test" != "$nb_test_limit" ]
    then
        echo " "
        echo -n "Do you want to run the test series again [y,n]? "
        read reply
        if [ "$reply" != "y" ]
        then
            break
        fi
    fi
    nb_test=$(($nb_test+1))
done

echo "--------------------------------------------------------------"
echo " "
echo -n "End of the test"
echo " "
