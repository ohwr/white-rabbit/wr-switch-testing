int ad9516_set_output_divider(int output, int ratio, int phase_offset);
int ad9516_init(void);
void ad9516_set_reg(int reg, int val);
int ad9516_read_reg(int reg);