/* PTS COM port test
 *
 * COM port replyer for WRS hardware PTS
 *
 *  Created on: January, 2017
 *  Authors:
 *         - INCAA Computers
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed for the WRS PTS
 *
 * You should have received a copy of the GNU General Public License...
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int main(int argc, char **argv)
{
    if (strcmp(argv[1], "CAFEBABE") == 0)
        printf("DEADBEEF\n");
    else
        printf("error\n");

	return 0;
}