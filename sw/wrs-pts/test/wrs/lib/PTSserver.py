#!   /usr/bin/env   python
#    coding: utf8

# Copyright CERN, 2017
# Author: INCAA Computers
# Licence: GPL v2 or later.
# Website: http://www.ohwr.org

import socket
import sys
import os
import io
import struct
import time
import threading
import serial
import select
from ctypes import *
from subprocess import *

"""        
Function for handling connections. This will be used to create threads
"""
def clientthread(conn, lib, stop_event):
    #Sending message to connected client
    conn.send('Welcome!\n')
    handler_stop = threading.Event()
    
    #loop untill error/close
    while (not stop_event.is_set()):
        try:
            #check if data available, otherwise wait on event
            rlist, _, _ = select.select([conn], [], [], 0.0)
            
            if not rlist:
                stop_event.wait(0.01)
            else:
                data = conn.recv(1024)
                
                #close on error
                if not data: 
                    break
                
                #if data, process
                data = data.strip()
                print 'WRS/DUT: Received: ' + data
                
                cmdlist = data.split()
                cmd = cmdlist[0]
                reply = ''
                
                #handle command
                if cmd == 'help' or cmd == 'h' or cmd == '-h' or cmd == '?' or cmd == '--help':
                    reply = (
                    '\nUse following commands args:\n' +
                    '- init\n' +
                    '- get_return_value\n' +
                    '- get_sfp_status\n' +
                    '- sfp_comm_test        port\n' +
                    '- set_fan_speed        enmask(0-3) speed(0=off 1=full speed)\n' +
                    '- get_temperatures\n' +
                    '- get_voltages\n' +
                    '- get_sfp_eeprom       port\n' +
                    '- set_ad9516_clk2      freq(0.004-250MHz) duty(0.001-0.999) cshift sigdel ppshift\n' +
                    '- set_ad9516           output(0-9) ratio(2-32(250-15MHz)) phase_offset(0-15)\n' +
                    '- init_ad9516\n' +
                    '- set_leds             color(0=off 1=green 2=orange)\n' +
                    '- write_reg_fpga       address value\n' +
                    '- read_reg_fpga        address\n' +
                    '- write_reg_ad9516     address value\n' +
                    '- read_reg_ad9516      address\n' +
                    '- write_gpio_control   pin value\n' +
                    '- read_gpio_status\n' +
                    '- get_button\n' +
                    '- write_gpio_rt        pin value\n' +
                    '- start_ttyS2_handler\n' +
                    '- stop_ttyS2_handler\n' +
                    '- exec_cmd_shell       command\n')
                elif cmd == 'init':
                    lib.init()
                    reply = 'ok'
                elif cmd == 'get_return_value':
                    reply = str(lib.get_return_value())
                elif cmd == 'get_sfp_status':
                    result1 = c_uint32(0);
                    result2 = c_uint32(0);
                    result3 = c_uint32(0);
                    lib.get_sfp_status(result1, result2, result3)
                    reply = str(result1.value) + ' ' + str(result2.value) + ' ' + str(result3.value)
                elif cmd == 'sfp_comm_test':
                    lib.sfp_comm_test(int(cmdlist[1]))
                    reply = 'ok'
                elif cmd == 'set_fan_speed':
                    lib.set_fan_speed(int(cmdlist[1]), float(cmdlist[2]))
                    reply = 'ok'
                elif cmd == 'get_temperatures':
                    result = (c_float * 6)()
                    lib.get_temperatures(result)
                    reply = '%.3f %.3f %.3f %.3f %.3f %.3f' % \
                    (result[0], result[1], result[2], result[3], result[4], result[5]) 
                    #fpga/pll/psl/psr/ic7/fpga_die
                elif cmd == 'get_voltages':
                    result = (c_float * 8)()
                    lib.get_voltages(result)
                    reply = '%.3f %.3f %.3f %.3f %.3f %.3f %.3f %.3f' % \
                    (result[0], result[1], result[2], result[3], result[4], result[5], result[6], result[7]) 
                    #VCCint/VCCaux/1V5/3V3/5VCCin/1V8/1V0gtx/1V2gtx
                elif cmd == 'get_sfp_eeprom':
                    result = (c_char * 96)()
                    lib.get_sfp_eeprom(int(cmdlist[1]), result)
                    reply = '%s %s %s' % (result[20:35].strip().replace(' ', '_'), result[40:55].strip(), result[68:83].strip()) 
                    #vn/pn/vs
                elif cmd == 'set_ad9516_clk2':
                    lib.set_ad9516_clk2(float(cmdlist[1]), float(cmdlist[2]), int(cmdlist[3]), int(cmdlist[4]), int(cmdlist[5]))
                    reply = 'ok'
                elif cmd == 'set_ad9516':
                    lib.set_ad9516(int(cmdlist[1]), int(cmdlist[2]), int(cmdlist[3]))
                    reply = 'ok'
                elif cmd == 'init_ad9516':
                    lib.init_ad9516()
                    reply = 'ok'
                elif cmd == 'set_leds':
                    lib.set_leds(int(cmdlist[1]))
                    reply = 'ok'
                elif cmd == 'write_reg_fpga':
                    lib.write_reg_fpga(int(cmdlist[1]), int(cmdlist[2]))
                    reply = 'ok'
                elif cmd == 'read_reg_fpga':
                    result = c_uint32(0)
                    lib.read_reg_fpga(int(cmdlist[1]), result)
                    reply = str(result.value)
                elif cmd == 'write_reg_ad9516':
                    lib.write_reg_ad9516(int(cmdlist[1]), int(cmdlist[2]))
                    reply = 'ok'
                elif cmd == 'read_reg_ad9516':
                    result = c_uint32(0)
                    lib.read_reg_ad9516(int(cmdlist[1]), result)
                    reply = str(result.value)
                elif cmd == 'write_gpio_control':
                    lib.write_gpio_control(int(cmdlist[1]), int(cmdlist[2]))
                    reply = 'ok'
                elif cmd == 'read_gpio_status':
                    result = c_uint32(0)
                    lib.read_gpio_status(result)
                    reply = str(result.value)
                elif cmd == 'get_button':
                    result = c_uint32(0)
                    lib.get_button(result)
                    reply = str(result.value)
                elif cmd == 'write_gpio_rt':
                    lib.write_gpio_rt(int(cmdlist[1]), int(cmdlist[2]))
                    reply = 'ok'
                elif cmd == 'start_ttyS2_handler':
                    threading.Thread(target = ttyS2_handler, args = ('/dev/ttyS2', handler_stop)).start()
                    reply = 'ok'
                elif cmd == 'stop_ttyS2_handler':
                    handler_stop.set()
                    reply = 'ok'
                elif cmd == 'exec_cmd_shell':
                    command = cmdlist[1]
                    for i in range(len(cmdlist)-2):
                        command = command + ' ' + cmdlist[2+i]
                    if command.find(' > ') == -1:
                        command += ' > /wr/bin/cmd_output.txt'
                    command_c = c_char_p(command)
                    lib.exec_cmd_shell(command_c)
                    reply = 'ok'
                else:
                    reply = 'unrecognized command'
                    
                if reply == '' or lib.get_return_value() != 0:
                    reply = 'error:%d' % lib.get_return_value()
                
                conn.sendall(cmd + ' ' + reply + '\n')
        
        except Exception as e:
            print 'Exception occurred: %s' % e
            break
                

    #came out of loop
    try:
        print 'Close, wait for new connections' 
        conn.sendall('error' + '\n')
    except Exception as e:
        print 'Exception occurred: %s' % e
    finally:
        conn.close()
        handler_stop.set()

def main():
    HOST = ''    # Symbolic name meaning all available interfaces
    PORT = 6789    # Arbitrary non-privileged port
    lib = WRS()
    gateway = get_default_gateway()
    print 'Gateway: %s' % gateway
    
    #create listen socket
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.settimeout(1)
    
    #create send ip socket
    s2 = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s2.settimeout(1)
    
    #Bind listen socket to local host and port
    try:
        s.bind((HOST, PORT))
    except socket.error as msg:
        print 'Bind failed. Error Code : ' + str(msg[0]) + ' Message ' + msg[1]
        sys.exit()

    #Start listening on socket
    thread_stop = threading.Event()
    s.listen(10)
    print 'Now listening'
    
    try:
        while 1:
            #wait to accept a connection
            conn = ''
            try:
                conn, addr = s.accept()
            except socket.timeout:
                pass
            
            if conn:
                print 'Connected with ' + addr[0] + ':' + str(addr[1])
        
                #start new thread for handler
                threading.Thread(target = clientthread, args = (conn, lib, thread_stop)).start()
                        
            else:
                try:
                    s2.sendto('ip', (gateway, PORT))
                except Exception as e:
                    print 'exception: %s' % e
    
    except Exception as e:
        print 'exception: %s' % e
    
    finally:
        thread_stop.set()
        s.close()
        s2.close()
      
def get_default_gateway():
    """Read the default gateway directly from /proc."""
    with open("/proc/net/route") as fh:
        for line in fh:
            fields = line.strip().split()
            if fields[1] != '00000000' or not int(fields[3], 16) & 2:
                continue

            return socket.inet_ntoa(struct.pack("<L", int(fields[2], 16)))

"""
thread for testing ttyS2 port
"""
def ttyS2_handler(usb, stop_event):
    try:
        ser = serial.Serial(usb, 115200, timeout=0)
        while(not stop_event.is_set()):
            recv = ser.read(1)
            if recv != '':
                if recv.isupper():
                    ser.write(recv.lower())
                else:
                    ser.write(recv.upper())
            stop_event.wait(0.001)
    except Exception as e:
        print 'ttyS2_handler failed due to: %s' % e
        
    ser.close()

        
class WRS:
    def __init__(self):
        self.initialized = False
        self.return_value = 0 
        self.wrs = CDLL('/wr/bin/wrs_pts_lib.so') #load library with hardware functions
        
    def init(self):
        self.return_value = self.wrs.init()
        self.initialized = True

    def get_return_value(self):
        return self.return_value
        
    def get_sfp_status(self, presence, los_txfault, txdisable):
        if self.initialized == False:
            self.init()
        self.return_value = self.wrs.get_sfp_status(pointer(presence), pointer(los_txfault), pointer(txdisable))
        
    def sfp_comm_test(self, port):
        if self.initialized == False:
            self.init()
        self.return_value = self.wrs.sfp_comm_test(c_int(port))
        
    def set_fan_speed(self, enmask, rate):
        if self.initialized == False:
            self.init()
        self.return_value = self.wrs.shw_pwm_speed(c_int(enmask), c_float(rate))

    def get_temperatures(self, temperatures):
        if self.initialized == False:
            self.init()
        self.return_value = self.wrs.get_temps(pointer(temperatures))
        
    def get_voltages(self, voltages):
        if self.initialized == False:
            self.init()
        self.return_value = self.wrs.get_voltages(pointer(voltages))
    
    def get_sfp_eeprom(self, port, sfp_header):
        if self.initialized == False:
            self.init()
        self.return_value = self.wrs.get_eeprom_sfp(c_int(port), pointer(sfp_header))
        
    def set_ad9516_clk2(self, freq, duty, cshift, sigdel, ppshift):
        if self.initialized == False:
            self.init()
        self.return_value = self.wrs.set_ad9516_clk2(c_float(freq), c_float(duty), c_int(cshift), c_int(sigdel), c_int(ppshift))

    def set_ad9516(self, output, ratio, phase_offset):
        if self.initialized == False:
            self.init()
        self.return_value = self.wrs.set_ad9516(c_int(output), c_int(ratio), c_int(phase_offset))   

    def init_ad9516(self):
        if self.initialized == False:
            self.init()
        self.return_value = self.wrs.init_ad9516()        
    
    def set_leds(self, color):
        if self.initialized == False:
            self.init()
        self.return_value = self.wrs.set_led(c_int(color))
        
    def write_reg_fpga(self, address, value):
        if self.initialized == False:
            self.init()
        self.return_value = self.wrs.write_reg_fpga(address, value)
     
    def read_reg_fpga(self, address, value):
        if self.initialized == False:
            self.init()
        self.return_value = self.wrs.read_reg_fpga(address, pointer(value))
        
    def write_reg_ad9516(self, address, value):
        if self.initialized == False:
            self.init()
        self.return_value = self.wrs.write_reg_ad9516(address, value)
     
    def read_reg_ad9516(self, address, value):
        if self.initialized == False:
            self.init()
        self.return_value = self.wrs.read_reg_ad9516(address, pointer(value))
        
    def write_gpio_control(self, pin, value):
        if self.initialized == False:
            self.init()
        self.return_value = self.wrs.write_gpio_control(pin, value)
     
    def read_gpio_status(self, value):
        if self.initialized == False:
            self.init()
        self.return_value = self.wrs.read_gpio_status(pointer(value))
        
    def get_button(self, value):
        if self.initialized == False:
            self.init()
        self.return_value = self.wrs.get_button(pointer(value))
        
    def write_gpio_rt(self, pin, value):
        if self.initialized == False:
            self.init()
        self.return_value = self.wrs.write_gpio_rt(pin, value)
        
    def exec_cmd_shell(self, command):
        self.return_value = self.wrs.exec_cmd_shell(command)
        
if __name__ == '__main__' :
    main()