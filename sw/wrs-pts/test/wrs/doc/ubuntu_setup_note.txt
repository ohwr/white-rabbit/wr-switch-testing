This note explains how to setup an Ubuntu PC for a WhiteRabbitSwitch production test suite.

Ubuntu version must be 11.04 (2.6.38)
http://old-releases.ubuntu.com/releases/11.04/ubuntu-11.04-desktop-i386.iso

Add within synaptic the following repositories
    http://old-releases.ubuntu.com/ubuntu natty main+multiverse+universe+restricted

The following packages must be installed:
    git
    python
    python-dev
    python-pip
    sudo pip install pexpect numpy pyro4 ptyprocess
    dnsmasq
    nfs-kernel-server
    
Clone WR Switch Testing git repository and create a symlink to wrs-pts:
    cd ~
    git clone git://ohwr.org/white-rabbit/wr-switch-testing.git
    cd wr-switch-testing
    ln -s ~/wr-switch-testing/sw/wrs-pts ~/pts

Make sure these files are executable:
    ~/pts/test/wrs/python/flash-wrs
    ~/pts/test/wrs/python/hwinfo/gensdbfs
    ~/pts/test/wrs/python/hwinfo/wrs_hwinfo
    ~/pts/test/wrs/lib/pts_com_test
    ~/pts/wrs.sh
    ~/pts/wrs-select.sh
    ~/pts/wrs-non_destructive.sh

Prepare tftp directory
    sudo mkdir /tftpboot
    cd ~/pts/test/wrs/python
    sudo python test000.py

Configure dnsmasq (https://wiki.debian.org/HowTo/dnsmasq)
    edit /etc/dnsmasq.conf
    uncomment interface=eth0
    uncomment dhcp-range=x.x.x.x,x.x.x.x.x,12h (choose range)
    uncomment enable-tftp
    uncomment tftp-root=/tftpboot

Configure nfs
    edit /etc/exports
    add /tftpboot/ *(rw,no_subtree_check,no_root_squash)
   
Compile usb-loader
    cd ~/pts/test/wrs/python/usb-loader
    make
   
Compile usbtmc stuff:
    cd ~/pts/usbdriver
    make

Create a script file to launch the test program:
    cd ~
    touch run_ptsWRS.sh
    chmod +x run_ptsWRS.sh

Edit the script file as follow:
    #!/bin/sh
    echo " "
    echo " "
    echo "********************************************************************************"
    echo "*                     WhiteRabbitSwitch Test program                           *"
    echo "********************************************************************************"
    echo " "
    cd ~/pts && sudo ./wrs.sh

Create an alias to be able to launch the test program by typing "testWRS".
Add the following to ~/.bashrc
    alias testWRS='~/run_ptsWRS.sh'

Define bash as the default shell:
    chsh -s /bin/bash <username>

In "System > Preferences > Startup Applications" add the following to
automatically open a terminal at startup:
    Command: gnome-terminal
