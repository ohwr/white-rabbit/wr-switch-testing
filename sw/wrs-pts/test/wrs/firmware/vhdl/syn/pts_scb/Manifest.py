target = "xilinx"
action = "synthesis"

fetchto = "../../ip_cores"

#syn_tool = "ise"
#top_module = "scb_top_synthesis"
syn_device = "xc6vlx240t"
syn_grade = "-1"
syn_package = "ff1156"
syn_top = "pts_scb_top"
syn_project = "pts_scb.xise"

modules = { "local" : [ "../../top/pts_scb",
                        "../../ip_cores/general-cores",
                        "../../ip_cores/wr-cores" ] }
