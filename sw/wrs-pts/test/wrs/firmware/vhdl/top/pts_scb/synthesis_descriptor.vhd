-- package generated automatically by gen_sdbsyn.py script --
library ieee;
use ieee.std_logic_1164.all;
use work.wishbone_pkg.all;
package synthesis_descriptor is
constant c_sdb_repo_url : t_sdb_repo_url := (
repo_url => "git://ohwr.org/white-rabbit/wr-switch-hdl.git                  ");
constant c_sdb_top_syn_info : t_sdb_synthesis := (
syn_module_name => "WRS_18p         ",
syn_commit_id => "1d500455                        ",
syn_tool_name => "ISE     ",
syn_tool_version => x"00000146",
syn_date => x"20170120",
syn_username => "Rene Bakker    ");
constant c_sdb_general_cores_syn_info : t_sdb_synthesis := (
syn_module_name => "general-cores   ",
syn_commit_id => "9c2a6c16                        ",
syn_tool_name => "        ",
syn_tool_version => x"00000000",
syn_date => x"00000000",
syn_username => "               ");
constant c_sdb_wr_cores_syn_info : t_sdb_synthesis := (
syn_module_name => "wr-cores        ",
syn_commit_id => "3c2a6c72                        ",
syn_tool_name => "        ",
syn_tool_version => x"00000000",
syn_date => x"00000000",
syn_username => "               ");

end package;