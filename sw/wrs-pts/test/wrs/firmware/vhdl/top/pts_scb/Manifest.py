files = [
        "pts_scb_top.ucf", 
        "pts_scb_top.vhd", 
        "swcore_pll.vhd",
        "pts_scb_top_bare.vhd",
        "pts_wb_cpu_bridge.vhd",        
        #"wb_cpu_bridge.vhd",        
        "scb_top_sim.vhd", 
        "wrsw_top_pkg.vhd",
        "wrs_sdb_pkg.vhd",
        "synthesis_descriptor.vhd",
        "xwrsw_syst_mon.vhd",
        "syst_mon.vhd",
        "xwrsw_sandbox.vhd",
        "freq_wbgen2_pkg.vhd",
        "wb_freq_regs.vhd",
        "xwrsw_freq_measure.vhd"
        ];

modules = { "local" : [ "../../" ] };
