#!   /usr/bin/env   python
#    coding: utf8

# Copyright CERN, 2017
# Author: INCAA Computers
# License: GPL v2 or later.
# Website: http://www.ohwr.org

# Import toolbox
from utilFunctions import *

def main (default_directory='.'):

    #constants declaration
    TEST_NB = 3
    NrOfTests = 1
    Subject = 'initialization of the hardware'
    Multiple = False
    Critical = True
    
    thread_stop = threading.Event()
    
    #==================================================
    #print start text
    frameworkText = framework_Text(TEST_NB, NrOfTests, Subject, Multiple, Critical)
    
    try:
        #initialize error variable
        error = [0] * NrOfTests
        
        #open socket
        conn = socket_init()
        
        #use debug port for logging
        threading.Thread(target = uart_log_thread, args = (thread_stop,)).start()

        #==================================================
        #try initialize hardware
        printAction('Trying to initialize hardware on DUT')
        error[0],_ = send_cmd_wrs(conn, 'init', '')
        
    #==================================================
    except Exception as e:
        raise PtsError("Test Failed: %s" % e)
        
    finally:
        if 'thread_stop' in locals():
            thread_stop.set()
        if 'conn' in locals():
            conn.close()
    
    #==================================================
    #process result
    if(error[0] != 0):
        if error[0] == -1:
            part = 'fpga memmap'
        elif error[0] == -2:
            part = 'fpga loaded check'
        elif error[0] == -3:
            part = 'io init'
        elif error[0] == -4:
            part = 'io configure'
        elif error[0] == -5:
            part = 'sfp buses init'
        elif error[0] == -6:
            part = 'fpga sensors i2c bus init'
        elif error[0] == -7:
            part = 'fpga bus0 i2c init'
        elif error[0] == -8:
            part = 'fpga bus1 i2c init'
        elif error[0] == -9:
            part = 'sfp gpio input init'
        elif error[0] == -10:
            part = 'fans init'
        else:
            part = 'unknown'
        
        print("Initialization of the hardware failed, error indicates part '%s'" % part)
    
    #print end text
    frameworkText.printEnd(error)
        
if __name__ == '__main__' :
    main()