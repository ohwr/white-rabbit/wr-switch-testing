#!   /usr/bin/env   python
#    coding: utf8

# Copyright CERN, 2017
# Author: INCAA Computers
# License: GPL v2 or later.
# Website: http://www.ohwr.org

"""
//Testing the dataflash: AT45DB642 
//
//	This test use mtd-tools (mtd_debug) to check all the dataflash
// 
// Page = 1056 bytes (1K + 32) => 0x420 (0x400+0x020) 
// Block = 8448 bytes (8K + 256) =>  8 x Pages => 0x2000
// Sector = 32 Blocks (Except Sector 0 = 1 x block (0); Sector [1-31] = 31 x blocks). 
// 
// Total: 32 sectors = 1024 blocks = 8192 pages = 8650752 bytes
//	: 0x84'0000 bytes

//The dataflash is partitioned in the following way:
//mtd2:0x0000.0000 - 0x0000.8400 33kB   at91boot
//mtd3:0x0000.8400 - 0x0008.c400 528kB  Barebox
//mtd4:0x0008.c400 - 0x0009.4800 33kB   Barebox-Environment
//mtd5:0x0009.4800 - 0x0009.5040 2112B  hwinfo
//mtd6:0x0009.5040 - 0x0084.0000 7851kB Available-dataflash
"""
    
# Import toolbox
from utilFunctions import *

def main (default_directory='.'):

    #constants declaration
    TEST_NB = 10
    NrOfTests = 7
    Subject = 'dataflash'
    Multiple = False
    
    thread_stop = threading.Event()
    results = [0,0,0,0,0]
    f2b = "/tmp/b-df.bin" #backup
    f2w = "/tmp/w-df.bin" #to write
    f2r = "/tmp/r-df.bin" #read
    cmd = ''
    device = ["/dev/mtd2", "/dev/mtd3", "/dev/mtd4", "/dev/mtd5", "/dev/mtd6"]
    start = [0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000]
    size = [33792, 540672, 33792, 2112, 8040384]
    timeout = [20.0, 40.0, 20.0, 20.0, 360.0]
    
    #==================================================
    #print start text
    frameworkText = framework_Text(TEST_NB, NrOfTests, Subject, Multiple)
    
    try:
        #initialize error variable
        error = [0] * NrOfTests
        
        #check if root
        if os.geteuid() != 0:
            print 'Sorry, I must be run as root'
            raise PtsError('Sorry, I must be run as root')
            
        #open socket
        conn = socket_init()
        
        #use debug port for logging for rest
        threading.Thread(target = uart_log_thread, args = (thread_stop,)).start()

        #==================================================
        #check mtd6 size
        printAction('Checking mtd6 size')
        cmd = 'mtd_debug info /dev/mtd6'
        error[0],_ = send_cmd_wrs(conn, 'exec_cmd_shell', cmd)
        with open(cmd_output, 'r') as f:
            while True:
                line = f.readline()
                if line.find('mtd.size = ') != -1:
                    size[4] = int(re.search(r'\d+', line).group())
                    print 'Detected size of mtd6 is %d' % size[4]
                    break
        
        #==================================================
        #add mtd5 (hwinfo write)
        printAction('Adding hwinfo write device')
        cmd = 'test -c /dev/mtd5 || mknod /dev/mtd5 c 90 10'
        error[0],_ = send_cmd_wrs(conn, 'exec_cmd_shell', cmd)
        with open(cmd_output, 'r') as f:
            print f.read()
            
        #==================================================
        #test all partitions
        for i in range(5):
            printAction('Testing partition %s' % device[i])
            
            #create file with random data to write
            print 'Create file with random data to write'
            cmd = "dd bs=%d count=1 if=/dev/urandom of=%s" % (size[i], f2w)
            send_cmd_wrs(conn, 'exec_cmd_shell', cmd, timeout[i])
            with open(cmd_output, 'r') as f:
                print f.read()
                
            #read current content and backup
            print 'Read and backup current contents'
            cmd = "mtd_debug read %s %d %d %s" % (device[i], start[i], size[i], f2b)
            send_cmd_wrs(conn, 'exec_cmd_shell', cmd, timeout[i])
            with open(cmd_output, 'r') as f:
                print f.read()
            
            #erase partition
            print 'Erase partition'
            cmd = "mtd_debug erase %s %d %d" % (device[i], start[i], size[i])
            send_cmd_wrs(conn, 'exec_cmd_shell', cmd, timeout[i])
            with open(cmd_output, 'r') as f:
                print f.read()
                
            #write test data
            print 'Write test file to partition'
            cmd = "mtd_debug write %s %d %d %s" % (device[i], start[i], size[i], f2w)
            send_cmd_wrs(conn, 'exec_cmd_shell', cmd, timeout[i])
            with open(cmd_output, 'r') as f:
                print f.read()

            #read written test data
            print 'Read back written test file'
            cmd = "mtd_debug read %s %d %d %s" % (device[i], start[i], size[i], f2r)
            send_cmd_wrs(conn, 'exec_cmd_shell', cmd, timeout[i])
            with open(cmd_output, 'r') as f:
                print f.read()
                
            #erase device
            print 'Erase partition'
            cmd = "mtd_debug erase %s %d %d" % (device[i], start[i], size[i])
            send_cmd_wrs(conn, 'exec_cmd_shell', cmd, timeout[i])
            with open(cmd_output, 'r') as f:
                print f.read()
            
            #write backup to partition
            print 'Write backup back to partition'
            cmd = "mtd_debug write %s %d %d %s" % (device[i], start[i], size[i], f2b)
            send_cmd_wrs(conn, 'exec_cmd_shell', cmd, timeout[i])
            with open(cmd_output, 'r') as f:
                print f.read()
                
            #compare written testdata with read testdata
            print 'Compare written/read test file'
            cmd = "cmp -l %s %s" % (f2w, f2r)
            error[1+i],_ = send_cmd_wrs(conn, 'exec_cmd_shell', cmd, timeout[i])
            with open(cmd_output, 'r') as f:
                print f.read()
                
        #==================================================
        #remove mtd5 (hwinfo write)
        printAction('Remove hwinfo write device')
        cmd = 'rm /dev/mtd5'
        error[6],_ = send_cmd_wrs(conn, 'exec_cmd_shell', cmd)
        with open(cmd_output, 'r') as f:
            print f.read()
        
    #==================================================
    except Exception as e:
        raise PtsError("Test Failed: %s" % e)
        
    finally:
        if 'thread_stop' in locals():
            thread_stop.set()
        if 'conn' in locals():
            conn.close()
    
    #==================================================
    #process result
    if(error[0] != 0):
        print 'Adding hwinfo write device failed'
    for i in range(5):
        if (error[1+i] != 0):
            print 'Testing of partition %s failed with cmp output %d' % (device[i], error[1+i])
    if(error[6] != 0):
        print 'Removing hwinfo write device failed'
            
    #print end text
    frameworkText.printEnd(error)
        
if __name__ == '__main__' :
    main()