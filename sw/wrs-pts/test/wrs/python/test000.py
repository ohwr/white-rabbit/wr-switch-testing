#!   /usr/bin/env   python
#    coding: utf8

# Copyright CERN, 2017
# Author: INCAA Computers
# License: GPL v2 or later.
# Website: http://www.ohwr.org

# Import toolbox
from utilFunctions import *

def main (default_directory='.'):

    #constants declaration
    TEST_NB = 0
    NrOfTests = 1
    Subject = 'preparing nfs boot'
    Multiple = False
    
    srcFW = '%s/../firmware' % default_directory
    srcLib = '%s/../lib' % default_directory
    pathDst = '/tftpboot'
    dstLib = '%s/rootfs/wr/bin/' % pathDst
    dstFW = '%s/rootfs/wr/lib/firmware/' % pathDst
    dstDotconfig = '%s/rootfs/wr/etc/' % pathDst
    dstInittab = '%s/rootfs/etc/inittab' % pathDst
    libs = ['PTS_com_test', 'PTSserver.py', 'wrs_pts_lib.so']
    confs = ['wrboot', 'dot-config']
    fw = ['18p_mb-LX240T.bin', 'rt_cpu.elf']
    
    #==================================================
    #print start text
    frameworkText = framework_Text(TEST_NB, NrOfTests, Subject, Multiple)
    
    try:
        #initialize error variable
        error = [0] * NrOfTests
        
        #check if root
        if os.geteuid() != 0:
            print 'Sorry, I must be run as root'
            raise PtsError('Sorry, I must be run as root')

        #==================================================
        #copy zImage to nfs dir
        print 'Copy zImage from %s to %s' % (srcFW, pathDst)
        print subprocess.check_output(['cp', '%s/zImage' % srcFW, pathDst])
        
        #create rootfs
        print 'Create rootfs dir'
        print subprocess.check_output(['mkdir', '%s/rootfs' % pathDst])
        
        #unzip to rootfs
        print 'Unzip content into rootfs'
        print subprocess.check_output(['tar', '-xvzf', '%s/wrs-image.tar.gz' % srcFW, \
        '-C', '%s/rootfs/' % pathDst])
        
        #copy config files into nfs dir
        print 'Copy config files into nfs'
        print subprocess.check_output(['cp', '%s/%s' % (srcFW, confs[0]), '%s/' % pathDst])
        print subprocess.check_output(['cp', '%s/%s' % (srcFW, confs[1]), dstDotconfig])
            
        #copy pts files into rootfs
        print 'Copy PTS files into rootfs'
        for i in range(len(libs)):
            print subprocess.check_output(['cp', '%s/%s' % (srcLib, libs[i]), dstLib])
        
        #edit inittab
        print 'Edit inittab for PTS start on boot'
        with open(dstInittab, 'a') as f:
            f.write('::respawn:python /wr/bin/PTSserver.py')
            
        #copy fw files into rootfs
        print 'Copy fw files into rootfs'
        for i in range(len(fw)):
            print subprocess.check_output(['cp', '%s/%s' % (srcFW, fw[i]), dstFW])
        
    #==================================================    
    except subprocess.CalledProcessError as e:
        raise PtsError("Test Failed: command '%s' gave result %d with description '%s'" 
        % (e.cmd, e.returncode, e.output))
    
    except Exception as e:
        raise PtsError("Test Failed: %s" % e)
        
    finally:
        print 'finished'
    
    #==================================================
    #process result
    if error[0] != 0:
        print 'error'
    
    #print end text
    frameworkText.printEnd(error)

if __name__ == '__main__' :
    main()