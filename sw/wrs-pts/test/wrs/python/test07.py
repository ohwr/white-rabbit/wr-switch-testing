#!   /usr/bin/env   python
#    coding: utf8

# Copyright CERN, 2017
# Author: INCAA Computers
# License: GPL v2 or later.
# Website: http://www.ohwr.org

# Import toolbox
from utilFunctions import *

def main (default_directory='.'):

    #constants declaration
    TEST_NB = 7
    NrOfTests = 2
    Subject = 'general button'
    Multiple = False
    
    red = redirect(0)
    thread_stop = threading.Event()
        
    #==================================================
    #print start text
    frameworkText = framework_Text(TEST_NB, NrOfTests, Subject, Multiple)
    
    try:
        #initialize error variable
        error = [0] * NrOfTests
        
        #open socket
        conn = socket_init()
        
        #use debug port for logging
        threading.Thread(target = uart_log_thread, args = (thread_stop,)).start()

        #==================================================
        #test button
        printAction('test off')
        ask_user_but(red, 'Please do not press the general button on the backside', conn, 1)
        _,result = send_cmd_wrs(conn, 'get_button', '')
        if (int(result[1]) != 1):
            error[0] = 1
        
        printAction('test on')
        ask_user_but(red, 'Please press and hold the general button on the backside', conn, 0)
        _,result = send_cmd_wrs(conn, 'get_button', '')
        if (int(result[1]) != 0):
            error[1] = 1
            
        printAction('test off')
        ask_user_but(red, 'Please release the general button on the backside', conn, 1)
        _,result = send_cmd_wrs(conn, 'get_button', '')
        if (int(result[1]) != 1):
            error[0] = 1
        
    #==================================================    
    except Exception as e:
        raise PtsError("Test Failed: %s" % e)
        
    finally:
        if 'thread_stop' in locals():
            thread_stop.set()
        if 'conn' in locals():
            conn.close()
            
    #==================================================
    #process result
    if(error[0] != 0):
        print 'General button is faulty, short circuit?'
    if(error[1] != 0):
        print 'General button is faulty, open circuit?'
        
    #print end text
    frameworkText.printEnd(error)

if __name__ == '__main__' :
    main()