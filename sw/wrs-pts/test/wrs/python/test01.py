#!   /usr/bin/env   python
#    coding: utf8

# Copyright CERN, 2017
# Author: INCAA Computers
# License: GPL v2 or later.
# Website: http://www.ohwr.org

# Import toolbox
from utilFunctions import *

def main (default_directory='.'):

    #constants declaration
    TEST_NB = 1
    NrOfTests = 2
    Subject = 'DDR2 RAM'
    Multiple = False
    Critical = True
    
    thread_stop = threading.Event()
    logfile = '/tmp/pexpect.log'
    timeout_serial = 120
    srcConfig = '%s/../firmware/config-nfs' % default_directory
    srcHwinfo = '%s/../firmware/default_hwinfo' % default_directory
    dstPath = '/tftpboot/'
        
    #==================================================
    #print start text
    frameworkText = framework_Text(TEST_NB, NrOfTests, Subject, Multiple, Critical)
    
    try:
        #initialize error variable
        error = [0] * NrOfTests
        
        #check if root
        if os.geteuid() != 0:
            print 'Sorry, I must be run as root'
            raise PtsError('Sorry, I must be run as root')
            
        #replace or create logfile, then start log thread
        log = open(logfile, 'w+')
        threading.Thread(target = file_log_thread, args = (logfile, thread_stop)).start()
    
        #==================================================
        #copy conf files into nfs dir
        printAction('Copy %s to %s' % (srcConfig, dstPath))
        print subprocess.check_output(['cp', srcConfig, dstPath])
        
        printAction('Copy %s to %s' % (srcHwinfo, dstPath))
        print subprocess.check_output(['cp', srcHwinfo, dstPath])
        
        #==================================================
        #get serial port
        ser = serial.Serial(get_arm_usb(), 115200, timeout=timeout_serial)
        
        #open pexpect task
        reader = fdspawn(ser, 'wb')
        reader.logfile = log
        reader.timeout = timeout_serial

        #==================================================
        #wait for started
        reader.sendline('')
        printAction('Waiting for Command Prompt')
        reader.expect('#')
        
        #reboot to barebox shell
        printAction('Reboot to barebox shell')
        reader.sendline('reboot')
        reader.expect('5: reboot')
        time.sleep(1)
        reader.send('4')
        time.sleep(1)
        reader.sendline('')
        
        #==================================================
        #wait for started
        printAction('Waiting for Command Prompt')
        reader.expect('#')
        
        #do memtest
        printAction('Start memtest')
        reader.sendline('memtest')
        reader.expect('wrs-([0-9A-Fa-f]{2}[:-]){5}([0-9A-Fa-f]{2})#')
        
        #==================================================
        #install config with nfs boot default
        printAction('Change default boot option to NFS boot')
        reader.sendline('rm /env/config')
        reader.expect('wrs-([0-9A-Fa-f]{2}[:-]){5}([0-9A-Fa-f]{2})#')
        reader.sendline('dhcp 5') 
        reader.expect('wrs-([0-9A-Fa-f]{2}[:-]){5}([0-9A-Fa-f]{2})#')
        reader.sendline('tftp config-nfs /env/config')
        reader.expect('wrs-([0-9A-Fa-f]{2}[:-]){5}([0-9A-Fa-f]{2})#')
        reader.sendline('saveenv')
        reader.expect('wrs-([0-9A-Fa-f]{2}[:-]){5}([0-9A-Fa-f]{2})#')
        
        #==================================================
        #install default hwinfo
        printAction('Install default hwinfo')
        reader.sendline('erase /dev/dataflash0.hwinfo')
        reader.expect('wrs-([0-9A-Fa-f]{2}[:-]){5}([0-9A-Fa-f]{2})#')
        reader.sendline('tftp default_hwinfo /dev/dataflash0.hwinfo')
        reader.expect('wrs-([0-9A-Fa-f]{2}[:-]){5}([0-9A-Fa-f]{2})#')
        
        #==================================================
        #restart
        printAction('Restart')
        reader.sendline('reset')
        time.sleep(1)
        try:
            reader.expect('\nwrs[\x21-\x7E]*#')
        except Exception as e:
            print 'Exception occured on wait restart: %s' % e
            error[1] = 1
        
        #==================================================
        #check memtest
        printAction('Check result')
        with open(logfile, 'r') as f:
            if f.read().find('Memtest successful') == -1:
                error[0] = 1

    #==================================================
    except subprocess.CalledProcessError as e:
        raise PtsError("Test Failed: command '%s' gave result %d with description '%s'" 
        % (e.cmd, e.returncode, e.output))
        
    except Exception as e:
        raise PtsError("Test Failed: %s" % e)
        
    finally:
        if 'thread_stop' in locals():
            thread_stop.set()
        if 'reader' in locals():
            reader.close()
        if 'log' in locals():
            log.close()
            
    #==================================================
    #process result
    if(error[0] != 0):
        print 'MEMTEST of DDR2 was not successful'
    if(error[1] != 0):
        print 'Timed out waiting on restart'
    
    #print end text
    frameworkText.printEnd(error)   
 
if __name__ == '__main__' :
    main()