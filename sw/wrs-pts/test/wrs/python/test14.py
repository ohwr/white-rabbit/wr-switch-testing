#!   /usr/bin/env   python
#    coding: utf8

# Copyright CERN, 2017
# Author: INCAA Computers
# License: GPL v2 or later.
# Website: http://www.ohwr.org

# Import toolbox
from utilFunctions import *

def main (default_directory='.'):

    #constants declaration
    TEST_NB = 14
    NrOfTests = 4
    Subject = 'SMC connectors'
    Multiple = True
    
    red = redirect(0)
    thread_stop = threading.Event()
    usb_tty = find_usb_tty.CttyUSB()
    usb_tmc = find_usb_tmc.CtmcUSB()
    freq = [31.25, 83.3, 10.0]
    margin = [0.1, 0.1, 0.1]
    
    #==================================================
    #print start text
    frameworkText = framework_Text(TEST_NB, NrOfTests, Subject, Multiple)
    
    try:
        #initialize error variable
        error = [0] * NrOfTests
        
        #check if root
        if os.geteuid() != 0:
            print 'Sorry, I must be run as root'
            raise PtsError('Sorry, I must be run as root')
            
        #open socket
        conn = socket_init()
        
        #use debug port for logging for rest
        threading.Thread(target = uart_log_thread, args = (thread_stop,)).start()

        #==================================================
        #init CNT-91
        printAction('Initializing CNT-91 counter')
        usb_port = usb_tmc.find_usb_tmc(CNT91_USB_VENDOR_ID, CNT91_USB_PRODUCT_ID)
        try:
            meas = PendulumCNT91(usb_port[0])
        except OSError as e:
            print e
            raise PtsError('CNT-91 is not switched on or badly connected, aborting.')

        #==================================================
        #test pps in/out
        #off
        printAction('Testing PPS in/out, now off')
        
        send_cmd_wrs(conn, 'write_gpio_control', '19 0')
        
        _,value = send_cmd_wrs(conn, 'read_gpio_status', '')
        status = int(value[1])
        print 'GPIO status: %d and pps in: %d' % (status, (status & 1<<19) != 0)
        if (status & 1<<19) != 0:
            error[0] = 1
            
        #on
        printAction('Testing PPS in/out, now on')
        
        send_cmd_wrs(conn, 'write_gpio_control', '19 1')
        
        _,value = send_cmd_wrs(conn, 'read_gpio_status', '')
        status = int(value[1])
        print 'GPIO status: %d and pps in: %d' % (status, (status & 1<<19) != 0)
        if (status & 1<<19) != 1<<19:
            error[0] += 1
            
        #off
        printAction('Testing PPS in/out, now off')
        
        send_cmd_wrs(conn, 'write_gpio_control', '19 0')
        
        _,value = send_cmd_wrs(conn, 'read_gpio_status', '')
        status = int(value[1])
        print 'GPIO status: %d and pps in: %d' % (status, (status & 1<<19) != 0)
        if (status & 1<<19) != 0:
            error[0] += 1
        #==================================================
        #test CLK1
        printAction('Testing CLK1 output on %.3fMHz' % freq[0])
        
        #setup pendulum
        pendulum_init(meas, 1)
        
        #set clk1
        send_cmd_wrs(conn, 'set_ad9516', '9 16 0')
            
        #restart CNT-91
        pendulum_reset(meas)
        
        #read result and check
        time.sleep(2)
        meas.write("CALC:AVER:COUN:CURR?")
        n = meas.read()
        if (convertString(n) != 100):
            error[1] += 1
            print "CLK1 is not switching on"
        
        #read data and store
        meas.write("CALC:DATA?")
        res = meas.read()
        value = convertString(res)
        if (value != 0):
            value = (value/1000000.0) #mhz
            print 'Measured freq on CLK1 is %.3fMHz' % value
            if (value > (freq[0] + margin[0]) or (value < (freq[0] - margin[0]))):
                error[1] += 1
                
        #==================================================
        #test CLK2
        printAction('Testing CLK2 output on %.3fMHz' % freq[1])
        
        #setup pendulum
        pendulum_init(meas, 2)
        
        #set clk2
        send_cmd_wrs(conn, 'set_ad9516_clk2', '83.3 0.5 0 0 0')
            
        #restart CNT-91
        pendulum_reset(meas)
        
        #read result and check
        time.sleep(2)
        meas.write("CALC:AVER:COUN:CURR?")
        n = meas.read()
        if (convertString(n) != 100):
            error[2] += 1
            print "CLK2 is not switching on"
        
        #read data and store
        meas.write("CALC:DATA?")
        res = meas.read()
        value = convertString(res)
        if (value != 0):
            value = (value/1000000.0) #mhz
            print 'Measured freq on CLK2 is %.3fMHz' % value
            if (value > (freq[1] + margin[1]) or (value < (freq[1] - margin[1]))):
                error[2] += 1
                
        #==================================================
        #test 10MHz in
        printAction('Testing 10MHz input on %.3fMHz' % freq[2])
        
        #setup agilent
        print 'Initializing Agilent signal generator'
        gen = agilent_init()
        time.sleep(2)
        
        _,result = send_cmd_wrs(conn, 'read_reg_fpga', '503812')
        value = 3 * int(result[1])
        value = (value/1000000.0) #mhz
        print 'Measured freq on 10MHz external in is %.3fMHz' % value
        if (value > (freq[2] + margin[2]) or (value < (freq[2] - margin[2]))):
            error[3] += 1
        
    #==================================================
    except Exception as e:
        raise PtsError("Test Failed: %s" % e)
        
    finally:
        if 'thread_stop' in locals():
            thread_stop.set()
        if 'conn' in locals():
            conn.close()
        if 'meas' in locals():
            meas.close()
        if 'gen' in locals():
            gen.output = False
            gen.close()
    
    #==================================================
    #process result
    if(error[0] != 0):
        print 'PPS input and/or output is not working correctly'
    if(error[1] != 0):
        print 'CLK1 output is not working correctly'
    if(error[2] != 0):
        print 'CLK2 output is not working correctly'
    if(error[3] != 0):
        print '10MHz input is not working correctly'
    
    #print end text
    frameworkText.printEnd(error)
        
if __name__ == '__main__' :
    main()