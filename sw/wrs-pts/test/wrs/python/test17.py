#!   /usr/bin/env   python
#    coding: utf8

# Copyright CERN, 2017
# Author: INCAA Computers
# License: GPL v2 or later.
# Website: http://www.ohwr.org

# Import toolbox
from utilFunctions import *
import hashlib

def main (default_directory='.'):

    #constants declaration
    TEST_NB = 17
    NrOfTests = 5
    Subject = 'flashing finalfw'
    Multiple = False

    sourcePath = '%s/../firmware/wrs-firmware-final.tar' % default_directory
    destinationPath = '/tftpboot/rootfs/wr/bin/wrs-firmware.tar'
    WRSsrcPath = '/wr/bin/wrs-firmware.tar'
    WRSdstPath = '/update/'
    logfile = '/tmp/pexpect.log'
    timeout_serial = 300
    thread_stop = threading.Event()
    parts = ['at91bootstrap.bin', 'barebox.bin', 'zImage', 'wrs-initramfs.gz', 'wrs-usr.tar.gz']
    srcConfig = '%s/../firmware/config-final' % default_directory
    dstConfig = '/tftpboot/config-final'
    
    #==================================================
    #print start text
    frameworkText = framework_Text(TEST_NB, NrOfTests, Subject, Multiple)
    
    try:
        #initialize error variable
        error = [0] * NrOfTests
        
        #check if root
        if os.geteuid() != 0:
            print 'Sorry, I must be run as root'
            raise PtsError('Sorry, I must be run as root')
            
        #check firmware file on right path
        if not (os.path.isfile(sourcePath)):
            raise PtsCritical('%s not found' % path)

        #open socket
        conn = socket_init()

        #==================================================
        #copy to nfs dir
        printAction('Copy %s to %s' % (sourcePath, destinationPath))
        print subprocess.check_output(['cp', sourcePath, destinationPath])
        
        #move to update folder
        printAction('Move from %s to %s on WRS' % (WRSsrcPath, WRSdstPath))
        error[0],_ = send_cmd_wrs(conn, 'exec_cmd_shell', 'mv %s %s' % (WRSsrcPath, WRSdstPath))
        with open(cmd_output, 'r') as f:
            print f.read()
            
        #reboot to update
        printAction('Reboot WRS to update')
        error[1],_ = send_cmd_wrs(conn, 'exec_cmd_shell', 'reboot')
        with open(cmd_output, 'r') as f:
            print f.read()
        
        #==================================================
        #init fdpexpect stuff
        #replace or create logfile, then start log thread
        log = open(logfile, 'w+')
        threading.Thread(target = file_log_thread, args = (logfile, thread_stop)).start()
        
        #get serial port
        ser = serial.Serial(get_arm_usb(), 115200, timeout=timeout_serial)
        
        #open pexpect task
        reader = fdspawn(ser, 'wb')
        reader.logfile = log
        reader.timeout = timeout_serial
        
        #==================================================
        #wait for completion
        time.sleep(1)
        printAction('Wait for completion')
        try:
            reader.expect('\nwrs[\x21-\x7E]*#')
        except Exception as e:
            print 'Exception occured on wait completion: %s' % e
            error[2] = 1
            
        #==================================================
        #check result
        printAction('Check result')
        
        #wait for start
        reader.sendline('')
        printAction('Waiting for Command Prompt')
        reader.expect('#')
        
        #search for checksum error file
        printAction('Check if checksum error file exists')
        reader.sendline('ls /update/')
        reader.sendline('')
        reader.expect('#')
        
        found = 0
        with open(logfile, 'r') as f:
            allLines = f.read()
            found = allLines.find('wrs-firmware.tar.checksum_error')
                
        if found != -1:
            print 'Checksum error found'
            error[3] += 1
        
        #check md5
        else:
            #wait for start
            reader.sendline('')
            printAction('Waiting for Command Prompt')
            reader.expect('#')
            
            #create md5 installed firmware
            printAction('Check MD5 installed firmware')
            reader.sendline('md5sum /update/current-wrs-firmware.tar')
            reader.expect('#')
            reader.sendline('')
            reader.expect('#')
            
            md5_installed = ''
            with open(logfile, 'r') as f:
                while True:
                    line = f.readline()
                    if line.find('/update/current-wrs-firmware.tar') != -1 and \
                    line.find('md5sum /update/current-wrs-firmware.tar') == -1:
                        md5_installed = re.search('[0-9a-f]+', line).group()
                        print 'MD5 installed firmware ' + md5_installed
                        break
                    
            #creat md5 local image
            printAction('Check MD5 local firmware')
            md5_local = hashlib.md5()
            with open(sourcePath, "rb") as f:
                for chunk in iter(lambda: f.read(4096), b""):
                    md5_local.update(chunk)
            print 'MD5 local firmware ' + md5_local.hexdigest()
                    
            #compare
            if md5_local.hexdigest() != md5_installed:
                print 'Not matching MD5'
                error[3] += 1

        #==================================================
        #copy config to nfs dir
        printAction('Copy %s to %s' % (srcConfig, dstConfig))
        print subprocess.check_output(['cp', srcConfig, dstConfig])
        
        #wait for start
        reader.sendline('')
        printAction('Waiting for Command Prompt')
        reader.expect('#')
        
        #reboot to barebox shell
        printAction('Reboot to barebox shell')
        reader.sendline('reboot')
        reader.expect('5: reboot')
        time.sleep(1)
        reader.send('4')
        time.sleep(1)
        reader.sendline('')
        
        #==================================================
        #wait for started
        printAction('Waiting for Command Prompt')
        reader.expect('#')
        
        #==================================================
        #set default config back
        printAction('Change default boot option to normal')
        reader.sendline('rm /env/config')
        reader.expect('wrs-([0-9A-Fa-f]{2}[:-]){5}([0-9A-Fa-f]{2})#')
        reader.sendline('dhcp 5') 
        reader.expect('wrs-([0-9A-Fa-f]{2}[:-]){5}([0-9A-Fa-f]{2})#')
        reader.sendline('tftp config-final /env/config')
        reader.expect('wrs-([0-9A-Fa-f]{2}[:-]){5}([0-9A-Fa-f]{2})#')
        reader.sendline('saveenv')
        reader.expect('wrs-([0-9A-Fa-f]{2}[:-]){5}([0-9A-Fa-f]{2})#')
        
        #restart
        printAction('Restart')
        reader.sendline('reset')
        time.sleep(1)
        try:
            reader.expect('\nwrs[\x21-\x7E]*#')
        except Exception as e:
            print 'Exception occured on wait restart: %s' % e
            error[4] = 1

    #==================================================
    except subprocess.CalledProcessError as e:
        raise PtsError("Test Failed: command '%s' gave result %d with description '%s'" 
        % (e.cmd, e.returncode, e.output))
        
    except Exception as e:
        raise PtsError("Test Failed: %s" % e)
        
    finally:
        if 'thread_stop' in locals():
            thread_stop.set()
        if 'reader' in locals():
            reader.close()
        if 'log' in locals():
            log.close()
            
    #==================================================
    #process result
    print ''
    if(error[0] != 0):
        print 'Copy of update tar to NFS failed with code %d' % error[0]
    if(error[1] != 0):
        print 'Reboot to update failed with code %d' % error[1]
    if(error[2] != 0):
        print 'Timed out while waiting on completion update'
    if(error[3] != 0):
        print 'Update was not successfull'
    if(error[4] != 0):
        print 'Timed out while waiting on boot from NAND'
    
    #print end text
    frameworkText.printEnd(error)

if __name__ == '__main__' :
    main()
