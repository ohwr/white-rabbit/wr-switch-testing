#!   /usr/bin/env   python
#    coding: utf8

# Copyright CERN, 2017
# Author: INCAA Computers
# License: GPL v2 or later.
# Website: http://www.ohwr.org

# Import toolbox
from utilFunctions import *

def main (default_directory='.'):

    #constants declaration
    TEST_NB = 4
    NrOfTests = 104
    Subject = 'CPU to FPGA bridge'
    Multiple = False
    
    thread_stop = threading.Event()
    reg_data = 0x0007FFE0;
    test_data = [0x00000000, 0xFFFFFFFF, 0xAAAAAAAA, 0x55555555];
    test_regs = [0x00040000, 0x0004FFFC, 0x0004AAA8, 0x00045554, \
                 0x0003FFFC, 0x0007FFFC, 0x0005FFFC, 0x0004FFFC];
        
    #==================================================
    #print start text
    frameworkText = framework_Text(TEST_NB, NrOfTests, Subject, Multiple)
    
    try:
        #initialize error variable
        error = [0] * NrOfTests
        
        #open socket
        conn = socket_init()
        
        #use debug port for logging for rest
        threading.Thread(target = uart_log_thread, args = (thread_stop,)).start()

        #==================================================
        #test address lines
        #patterns
        printAction('Testing address lines with patterns')
        for i in range(8):
            send_cmd_wrs(conn, 'write_reg_fpga', '%d 0' % test_regs[i])
            
            _,result = send_cmd_wrs(conn, 'read_reg_fpga', '%d' % test_regs[i])
            
            print 'Read: %x, expected: %x' % (int(result[1]), test_regs[i])
            
            if int(result[1]) != test_regs[i]:
                print 'Not OK'
                error[i] = 1
                
        #walking 0/1
        printAction('Testing address lines with walking 0/1')
        for y in range(2):
            for i in range(14):
                if y == 0:
                    testReg = 0x40000 + (~(1<<i+2)&0xFFFC)
                if y == 1:
                    testReg = 0x40000 + (1<<i+2)
                send_cmd_wrs(conn, 'write_reg_fpga', '%d 0' % testReg)
                
                _,result = send_cmd_wrs(conn, 'read_reg_fpga', '%d' % testReg)
                
                print 'Read: %x, expected: %x' % (int(result[1]), testReg)
                
                if int(result[1]) != testReg:
                    print 'Not OK'
                    error[i+8+(y*14)] = 1
                
        #==================================================
        #test data lines
        #patterns
        printAction('Testing data lines with patterns')
        for i in range(4):
            send_cmd_wrs(conn, 'write_reg_fpga', '%d %d' % (reg_data, test_data[i]))
            
            _,result = send_cmd_wrs(conn, 'read_reg_fpga', '%d' % reg_data)
            
            print 'Read: %x, expected: %x' % (int(result[1]), test_data[i])
            
            if int(result[1]) != test_data[i]:
                print 'Not OK'
                error[i+36] = 1
        
        #walking 0/1
        printAction('Testing data lines with walking 0/1')
        for y in range(2):
            for i in range(32):
                if y == 0:
                    testValue = ~(1<<i)&0xFFFFFFFF
                if y == 1:
                    testValue = (1<<i)
                send_cmd_wrs(conn, 'write_reg_fpga', '%d %d' % (reg_data, testValue))
                
                _,result = send_cmd_wrs(conn, 'read_reg_fpga', '%d' % reg_data)
                
                print 'Read: %x, expected: %x' % (int(result[1]), testValue)
                
                if int(result[1]) != testValue:
                    print 'Not OK'
                    error[i+40+(y*32)] = 1
        
    #==================================================
    except Exception as e:
        raise PtsError("Test Failed: %s" % e)
    
    finally:
        if 'thread_stop' in locals():
            thread_stop.set()
        if 'conn' in locals():
            conn.close()
    
    #==================================================
    #process result
    if(error[0:36] != [0] * 36):
        print 'Address lines are not connected correctly'
    if(error[36:104] != [0] * 68):
        print 'Data lines are not connected correctly'
    
    #print end text
    frameworkText.printEnd(error)
        
if __name__ == '__main__' :
    main()