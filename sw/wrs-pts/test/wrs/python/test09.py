#!   /usr/bin/env   python
#    coding: utf8

# Copyright CERN, 2017
# Author: INCAA Computers
# License: GPL v2 or later.
# Website: http://www.ohwr.org

# Import toolbox
from utilFunctions import *

def main (default_directory='.'):

    #constants declaration
    TEST_NB = 9
    NrOfTests = 13
    Subject = 'voltage levels'
    Multiple = True
    
    thread_stop = threading.Event()
    names = ['VCCint', 'VCCaux', '1V5', '3V3', '5VCCin', '1V8', '1V0gtx', '1V2gtx']
    level = [1.0, 2.5, 1.5, 3.3, 5.0, 1.8, 1.0, 1.2]
    percentage = [10.0, 10.0, 10.0, 10.0, 10.0, 10.0, 10.0, 10.0]
    margin = []
    for i in range(len(percentage)):
        margin.append(level[i] * (percentage[i] / 100.0))
    volts = [0,0,0,0,0,0,0,0]
    pg_pins = ['pg_1V0_gtx', 'pg_3V3_pll', 'pg_2V5_pll', 'pg_1V2_gtx', 'pg_3V3']
  
    #==================================================
    #print start text
    frameworkText = framework_Text(TEST_NB, NrOfTests, Subject, Multiple)
    
    try:
        #initialize error variable
        error = [0] * NrOfTests
        
        #open socket
        conn = socket_init()
        
        #use debug port for logging for rest
        threading.Thread(target = uart_log_thread, args = (thread_stop,)).start()

        #==================================================
        #check voltages
        printAction('Checking voltages')
        _,result = send_cmd_wrs(conn, 'get_voltages', '')
        
        for i in range(8):
            volts[i] = float(result[1+i])
            if (volts[i] > (level[i] + margin[i]) or (volts[i] < (level[i] - margin[i]))):
                print '%s not in range %.3f +/- %.3f' % (names[i], level[i], margin[i])
                error[i] = 1
        
        #print values
        text = ''
        for i in range(8):
            text += '%s: %.3fV%s' % (names[i], volts[i], '' if i==7 else ', ')
        print text   
        
        #==================================================
        #powerGood pins
        printAction('Checking PowerGood pins')
        status = 0x7F004
        _,value = send_cmd_wrs(conn, 'read_reg_fpga', '%d' % status)
        result = int(value[1])
        
        for i in range(5):
            if (result & 1<<(22+i)) == 0:
                print 'PowerGood pin %s was not ok' % pg_pins[i]
                error[8+i] = 1
        
        #print values
        text = ''
        for i in range(5):
            text += '%s: %d%s' % (pg_pins[i], (result &  1<<(22+i))!=0, '' if i==4 else ', ')
        print text  
  
    #==================================================
    except Exception as e:
        raise PtsError("Test Failed: %s" % e)
        
    finally:
        if 'thread_stop' in locals():
            thread_stop.set()
        if 'conn' in locals():
            conn.close()
    
    #==================================================
    #process result
    if(error[0:8] != [0] * 8):
        print 'One or more of the voltages is not in specified range'
    if(error[8:13] != [0] * 5):
        print 'One or more of the PowerGood pins indicates a bad voltage'

    #print end text
    frameworkText.printEnd(error)
        
if __name__ == '__main__' :
    main()