#!   /usr/bin/env   python
#    coding: utf8

# Copyright CERN, 2017
# Author: INCAA Computers
# License: GPL v2 or later.
# Website: http://www.ohwr.org

# Import toolbox
from utilFunctions import *

def main (default_directory='.'):

    #constants declaration
    TEST_NB = 8
    NrOfTests = 5
    Subject = 'SFP connections'
    Multiple = True
    
    red = redirect(0)
    thread_stop = threading.Event()
    fields = ['VendorName', 'ProductName']
    vendors = ['Molex_Inc.']
    pnames = ['74720-0501']
    results = [0,0,0]
    strings = ['','','']
    lines = ['Presence', 'LOS_TXfault', 'TXdisable']

    #==================================================
    #print start text
    frameworkText = framework_Text(TEST_NB, NrOfTests, Subject, Multiple)
    
    try:
        #initialize error variable
        error = [0] * NrOfTests
        
        #open socket
        conn = socket_init()
        
        #use debug port for logging for rest
        threading.Thread(target = uart_log_thread, args = (thread_stop,)).start()

        #==================================================
        #check SFP presence/los_txfault/txdisable lines
        printAction('Checking SFP control lines')
        _,result = send_cmd_wrs(conn, 'get_sfp_status', '')
        
        for y in range(18):
            for i in range(3):
                results[i] = int(result[1+i])
                if results[i] & (1 << y) == 0:
                    print '%s was not %d' % (lines[i], 1<<y)
                    error[i] += 1
        
        print '%s: %s, %s: %s, %s: %s' \
        % (lines[0], bin(results[0])[2:].zfill(18), lines[1], bin(results[1])[2:].zfill(18), \
        lines[2], bin(results[2])[2:].zfill(18))  

        #==================================================
        #check SFP communication
        for y in range(1000):
            print 'count = %d' % y
            time.sleep(1); #for random number
            value,_ = send_cmd_wrs(conn, 'sfp_comm_test', '13', 60)
            
            if value != 0:
                print 'The result of the communication is %d byte count (should be 0)' % value
                error[4] += 1
                break



            
    #==================================================
    except Exception as e:
        raise PtsError("Test Failed: %s" % e)
        
    finally:
        if 'thread_stop' in locals():
            thread_stop.set()
        if 'conn' in locals():
            conn.close()
    
    #==================================================
    #process result
    if(error[0] != 0):
        print '%d presence line(s) are faulty' % error[0]
    if(error[1] != 0):
        print '%d LOS/TXfault line(s) are faulty' % error[1]
    if(error[2] != 0):
        print '%d TXdisable line(s) are faulty' % error[2]
    if(error[3] != 0):
        print '%d eeprom field(s) were faulty (2 per port)' % error[3]
    if(error[4] != 0):
        print 'in %d port(s) the communication lines are faulty' % error[4]
        
    #print end text
    frameworkText.printEnd(error)
        
if __name__ == '__main__' :
    main()
