#!   /usr/bin/env   python
#    coding: utf8

# Copyright CERN, 2017
# Author: INCAA Computers
# License: GPL v2 or later.
# Website: http://www.ohwr.org

"""
// Testing the NAND flash: MT29F4G16 (x16)
// 
//	This test first try the different partitions by writing/reading random blocks
//	Then it format & mount a JFFS2 partition to write/read random size files.
//
// 
// Page	 = 1056 words = (1K + 32) words 
// Block = 64 x pages = (64K + 2K) words  =>  (0x0002.0000 + 0x0000.1000) bytes 
// Total =  4096 blocks = 262144 pages = 512 MBytes = 4.224Mbits
//		 = (0x2000.0000 + 0x0100.0000) bytes

//This is how the memory is used:
//mtd0:0x0000.0000 - 0x0010.0000 Barebox-environment-backup
//mtd1:0x0010.0000 - 0x2000.0000 UBIfied-NAND

//the UBI volumes stored within “UBIfied-NAND”:
//mtd7: 0201d800 0001f800 "boot"
//mtd8: 0961e000 0001f800 "usr"
//mtd9: 0961e000 0001f800 "update"
"""

# Import toolbox
from utilFunctions import *

def main (default_directory='.'):

    #constants declaration
    TEST_NB = 11
    NrOfTests = 5
    Subject = 'NAND'
    Multiple = False
    
    thread_stop = threading.Event()
    results = [0,0,0,0,0]
    f2w = '/tmp/w-df.bin' #to write
    f2r = '' #dynamic read name
    cmd = ''
    device = ["/dev/mtd0", "/dev/mtd1", "/boot", "/usr", "/update"]
    passes = [3, 1, 10, 10, 10]
    mins = [1, 30]
    steps = 4
    sum = 0
  
    #==================================================
    #print start text
    frameworkText = framework_Text(TEST_NB, NrOfTests, Subject, Multiple)
    
    try:
        #initialize error variable
        error = [0] * NrOfTests
        
        #check if root
        if os.geteuid() != 0:
            print 'Sorry, I must be run as root'
            raise PtsError('Sorry, I must be run as root')
            
        #open socket
        conn = socket_init()
        
        #use debug port for logging for rest
        threading.Thread(target = uart_log_thread, args = (thread_stop,)).start()
      
        #==================================================
        #test all partitions with nandtest application
        for i in range(2):
            for y in range(passes[i]):
                printAction('NANDtest for %s pass %d, takes up to %d minute(s)' % (device[i], y+1, mins[i]))
                
                cmd = "nandtest --markbad --keep %s" % device[i]
                result,_ = send_cmd_wrs(conn, 'exec_cmd_shell', cmd, 1800 if i==1 else 10)
                error[i] += result
                with open(cmd_output, 'r') as f:
                    print f.read()
        
        #==================================================
        #test read/write file
        for i in range(2, 5):
            for y in range(passes[i]):
                printAction('Testing with random file write/read for %s pass %d' % (device[i], y+1))
                
                #create filename
                f2r = "%s/r-df-%d.bin" % (device[i], y)
                
                #create file with random data to write
                size = 128 + random.randint(0, 1920)
                cmd = "dd bs=1k count=%d if=/dev/urandom of=%s" % (size, f2w)
                send_cmd_wrs(conn, 'exec_cmd_shell', cmd)
                with open(cmd_output, 'r') as f:
                    print f.read()
                    
                print "Testing: %s with size: %d kB\n" % (f2r, size)
                sum += size
                
                #write test data
                cmd = "dd bs=1k count=%d if=%s of=%s" % (size, f2w, f2r)
                send_cmd_wrs(conn, 'exec_cmd_shell', cmd)
                with open(cmd_output, 'r') as f:
                    print f.read()
                    
                #compare test data with written test data
                cmd = "cmp -l %s %s" % (f2w, f2r)
                result,_ = send_cmd_wrs(conn, 'exec_cmd_shell', cmd)
                error[i] += result
                with open(cmd_output, 'r') as f:
                    print f.read()
                    
                #remove test data
                cmd = "rm %s" % f2r
                send_cmd_wrs(conn, 'exec_cmd_shell', cmd)
                with open(cmd_output, 'r') as f:
                    print f.read()
                    
        printAction("Tested nand write/read files with %d MB of random data\n" % (sum/1024))
 
    #==================================================
    except Exception as e:
        raise PtsError("Test Failed: %s" % e)
        
    finally:
        if 'thread_stop' in locals():
            thread_stop.set()
        if 'conn' in locals():
            conn.close()

    #==================================================
    #process result
    for i in range(2):
        if(error[i] != 0):
            print 'NANDtest for %s failed' % device[i]
    for i in range(2, 5):
        if(error[i] != 0):
            print 'Test write/read file for %s failed' % device[i]
            
    #print end text
    frameworkText.printEnd(error)
        
if __name__ == '__main__' :
    main()
