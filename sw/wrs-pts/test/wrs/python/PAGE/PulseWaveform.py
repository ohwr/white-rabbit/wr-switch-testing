import Waveform
from Utilities import *
from numpy import *
import Pyro4
import Pyro4.util
import sys

class PulseWaveform(Waveform.Waveform):
    def get(self, what):
        return self.__getattribute__(what)
    
    def set(self, what, how):
        self.__setattr__(what, how)
    
    _parameters = {'frequency':['Frequency', 'Frequency of the pulse, in HZ', 1000, float],
                  'amplitude':['Amplitude', 'Amplitude of the pulse, in Vpp', 1, float],
                  'dc':['DC Compoment', 'DC component of the pulse, in Vpp', 0, float]}
                  
    def __init__(self, *args, **kwargs):
        Waveform.Waveform.__init__(self, *args, **kwargs)

name = 'Pulse Waveform'
target = PulseWaveform

import commands

def launch():
    g = target()
    hn = commands.getoutput('hostname')
    
    daemon=Pyro4.Daemon(host = hn)
    
    myUri = daemon.register(g)
    
    ns=Pyro4.locateNS()
    ns.register("Pulse", myUri)
    daemon.requestLoop()

if __name__ == '__main__':
    launch()