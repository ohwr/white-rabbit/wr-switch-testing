#!   /usr/bin/env   python
#    coding: utf8

# Copyright CERN, 2017
# Author: INCAA Computers
# License: GPL v2 or later.
# Website: http://www.ohwr.org

# Import toolbox
from utilFunctions import *

def main (default_directory='.'):

    #constants declaration
    TEST_NB = 6
    NrOfTests = 3
    Subject = 'LEDs'
    Multiple = True
    
    red = redirect(0)
    thread_stop = threading.Event()
        
    #==================================================
    #print start text
    frameworkText = framework_Text(TEST_NB, NrOfTests, Subject, Multiple)
    
    try:
        #initialize error variable
        error = [0] * NrOfTests
        
        #open socket
        conn = socket_init()
        
        #use debug port for logging
        threading.Thread(target = uart_log_thread, args = (thread_stop,)).start()

        #==================================================
        #test LEDs GREEN
        printAction('Put green leds on')
        send_cmd_wrs(conn, 'set_leds', '1')
        
        if (ask_user(red, 'Are the front panel LEDs (Power/Status/SFP cages) GREEN') == "N"):
            print 'Not all leds where green/on'
            error[0] = 1
        
        #==================================================
        #test LEDs ORANGE/RED
        printAction('Put orange/red leds on')
        send_cmd_wrs(conn, 'set_leds', '2')

        if (ask_user(red, 'Are the front panel LEDs on SFP cages ORANGE and Status RED') == "N"):
            print 'Not all leds where orange/red/on'
            error[1] = 1
            
        #==================================================
        #test LEDs OFF
        printAction('Put leds off')
        send_cmd_wrs(conn, 'set_leds', '0')

        if (ask_user(red, 'Are the front panel LEDs (Status/SFP cages) OFF') == "N"):
            print 'Not all leds where off'
            error[2] = 1

    #==================================================    
    except Exception as e:
        raise PtsError("Test Failed: %s" % e)
        
    finally:
        if 'thread_stop' in locals():
            thread_stop.set()
        if 'conn' in locals():
            conn.close()
            
    #==================================================
    #process result
    if(error[0] != 0):
        print 'One or more GREEN LEDs not switching ON'
    if(error[1] != 0):
        print 'One or more ORANGE LEDs not switching ON'
    if(error[2] != 0):
        print 'One or more LEDs not switching OFF'
        
    #print end text
    frameworkText.printEnd(error)

if __name__ == '__main__' :
    main()