# Copyright CERN, 2017
# Author: INCAA Computers
# Licence: GPL v2 or later.
# Website: http://www.ohwr.org

import os

class usbtmc:
    
    def __init__(self, device):
	print("Opening: %s" % device)
        self.device = device
        self.FILE = os.open(device, os.O_RDWR)
        
    def write(self, command):
        os.write(self.FILE, command)
        
    def read(self):
        return os.read(self.FILE, 4000)
    
    def getName(self):
        self.write("*IDN?")
        return self.read()
    
    def sendReset(self):
        self.write("*RST")
        
    def close(self):
        os.close(self.FILE)
        
class PendulumCNT91:
    
    def __init__(self, device):
        self.meas = usbtmc(device)
        self.name = self.meas.getName()
        print self.name
        
    def write(self, command):
        self.meas.write(command)
        
    def read(self):
        return self.meas.read()
    
    def reset(self):
        self.meas.sendReset()
        
    def close(self):
        self.meas.close()