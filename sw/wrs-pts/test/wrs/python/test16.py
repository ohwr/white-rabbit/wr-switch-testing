#!   /usr/bin/env   python
#    coding: utf8

# Copyright CERN, 2017
# Author: INCAA Computers
# License: GPL v2 or later.
# Website: http://www.ohwr.org

# Import toolbox
from utilFunctions import *

names = ['FPGA', 'PLL', 'PSL', 'PSR', 'Backplane', 'FPGA_DIE']
SafetyMax = 100.0
SafetyMin = 5.0

def print_check (data):
    
    values = [0.0,0.0,0.0,0.0,0.0,0.0]
    
    #print values
    text = ''
    for i in range(6):
        values[i] = float(data[1+i])
        text += '%s: %.3f°C%s' % (names[i], values[i], '' if i==5 else ', ')
    print text
    
    #check safety limits
    for i in range(6):
        if (values[i] > SafetyMax) or (values[i] < SafetyMin):
            print 'Temperature of %s is not between %d-%d°C' % (names[i], SafetyMin, SafetyMax)
            raise PtsError("Safety limit crossed")

    return values

def main (default_directory='.'):

    #constants declaration
    TEST_NB = 16
    NrOfTests = 8
    Subject = 'temperature sensors'
    Multiple = True
    
    thread_stop = threading.Event()
    start = [0,0,0,0,0,0]
    rise = [0,0,0,0,0,0]
    drop = [0,0,0,0,0,0]
    margin = [1.0, 2.0, 0.5, 1.0, 0.3, 2.0]
    margin2 = [0.5, 1.0, 0.1, 0.4, 0.1, 1.0] 
    minTemps = [51.0, 53.0, 36.0, 39.0, 25.0, 56.0]
    ok = False
    retries = 120 #5s per try
  
    #==================================================
    #print start text
    frameworkText = framework_Text(TEST_NB, NrOfTests, Subject, Multiple)
    
    try:
        #initialize error variable
        error = [0] * NrOfTests
        
        #open socket
        conn = socket_init()
        
        #use debug port for logging for rest
        threading.Thread(target = uart_log_thread, args = (thread_stop,)).start()
      
        #==================================================
        #check current temperatures
        printAction('Check current temperatures')
        _,result = send_cmd_wrs(conn, 'get_temperatures', '')
        start = print_check(result)
        
        #==================================================
        #set fans off
        printAction('Both fans off')
        send_cmd_wrs(conn, 'set_fan_speed', '3 0')
        
        #==================================================
        #check minimum temperatures
        printAction('Check minimum temperatures')
        
        #pause log tasks
        setPauseLog(True)
        
        for y in range(retries):
            time.sleep(5)
            _,result = send_cmd_wrs(conn, 'get_temperatures', '')
            start = print_check(result)

            #check minimum
            for i in range(6):
                if start[i] < minTemps[i]:
                    print '%s too low, threshold is %.3f' % (names[i], minTemps[i])
                    break
                if i == 5:
                    ok = True
            
            if ok:
                break
                
        #resume log tasks
        setPauseLog(False)
        
        #==================================================
        #set fans on
        printAction('Both fans on')
        send_cmd_wrs(conn, 'set_fan_speed', '3 1')
        
        #wait for stabilize
        time.sleep(60)
        
        #==================================================
        #check current temperatures
        printAction('Check current temperatures')
        _,result = send_cmd_wrs(conn, 'get_temperatures', '')
        start = print_check(result)
        
        #==================================================
        #set fans off
        printAction('Both fans off')
        send_cmd_wrs(conn, 'set_fan_speed', '3 0')

        #==================================================
        #check for rise
        printAction('Waiting for rise of temperature')
        
        #pause log tasks
        setPauseLog(True)
        
        ok = False
        for y in range(retries):
            time.sleep(5)
            _,result = send_cmd_wrs(conn, 'get_temperatures', '')
            rise = print_check(result)
            
            #check margins
            for i in range(6):
                if rise[i] < (start[i] + margin[i]):
                    print '%s too low, threshold is %.3f' % (names[i], start[i] + margin[i])
                    break
                if i == 5:
                    ok = True
            
            if ok:
                break
                
        #check result
        if ok == False:
            print 'Waiting for rise stopped, not correct'
            error[0] = 1
            
        #resume log tasks
        setPauseLog(False)
        
        #==================================================
        #set fans on
        printAction('Both fans on')
        send_cmd_wrs(conn, 'set_fan_speed', '3 1')

        #==================================================
        #check for drop
        printAction('Waiting for drop of temperature')
        
        #pause log tasks
        setPauseLog(True)
        
        ok = False
        for y in range(retries):
            time.sleep(5)
            _,result = send_cmd_wrs(conn, 'get_temperatures', '')
            drop = print_check(result)

            #check margins
            for i in range(6):
                if drop[i] > (rise[i] - margin2[i]):
                    print '%s too high, threshold is %.3f' % (names[i], rise[i] - margin2[i])
                    break
                if i == 5:
                    ok = True
            
            if ok:
                break
        
        #check result
        if ok == False:
            print 'Waiting for drop stopped, not correct'
            error[1] = 1
            
        #resume log tasks
        setPauseLog(False)

    #==================================================
    except Exception as e:
        raise PtsError("Test Failed: %s" % e)
        
    finally:
        #resume log tasks
        setPauseLog(False)
        
        if 'thread_stop' in locals():
            thread_stop.set()
        if 'conn' in locals():
            #set fans on
            printAction('Both fans on')
            send_cmd_wrs(conn, 'set_fan_speed', '3 1')
            conn.close()

    #==================================================
    #process result
    if(error[0] != 0):
        print 'Temperature rise was not seen for all sensors'
    if(error[1] != 0):
        print 'Temperature drop was not seen for all sensors'
      
    #print end text
    frameworkText.printEnd(error)
        
if __name__ == '__main__' :
    main()
