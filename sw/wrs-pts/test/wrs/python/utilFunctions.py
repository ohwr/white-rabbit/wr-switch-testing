# Copyright CERN, 2017
# Author: INCAA Computers
# License: GPL v2 or later.
# Website: http://www.ohwr.org

#Add common modules and libraries location to path
#sys.path.append('../../../')
#os.path.join(default_directory, path)

from ptsexcept import *
from PendulumCNT91 import *
from PAGE.Agilent33250A import *
from PAGE.PulseWaveform import *
from pexpect.fdpexpect import *

import sys
import os
import struct
import socket
import find_usb_tty
import find_usb_tmc
import time
import subprocess
import threading
import serial
import select
import re
import random

# USB ports vendor and product IDs
MGMT_USB_VENDOR_ID = 0x0525 # Netchip Technology, Inc.
MGMT_USB_PRODUCT_ID = 0xa4a7 # Linux-USB Serial Gadget (CDC ACM mode)
ARM_FPGA_USB_VENDOR_ID = 0x10c4 # Cygnal Integrated Products, Inc.
ARM_FPGA_USB_PRODUCT_ID = 0xea60 # CP210x Composite Device
BOOTL_USB_VENDOR_ID = 0x03eb # Atmel Corp.
BOOTL_USB_PRODUCT_ID = 0x6124 # at91sam SAMBA bootloader
RS232_USB_VENDOR_ID = 0x0403 # Future Technology Devices International, Ltd
RS232_USB_PRODUCT_ID = 0x6001 # FT232 USB-Serial (UART) IC
CNT91_USB_VENDOR_ID = 0x14eb # pendulum
CNT91_USB_PRODUCT_ID = 0x0091 # cnt-91
AGILENT_USB_VENDOR_ID = 0x067b # Prolific Technology, Inc.
AGILENT_USB_PRODUCT_ID = 0x2303 # PL2303 Serial Port

#globals
cmd_output = '/tftpboot/rootfs/wr/bin/cmd_output.txt'
pauseLog = False
arm_usb = ''

def setPauseLog(value):
    global pauseLog
    pauseLog = value

def arm_usb():
    return arm_usb

"""
standard in/out temporary redirection for user question/answer
"""

class redirect:
    def __init__(self, init):
        self.redirected = init
        
    def InOut(self):
        if(self.redirected == 0):
            self.tmp_stdout = sys.stdout
            sys.stdout = sys.__stdout__
            self.tmp_stdin = sys.stdin
            sys.stdin = sys.__stdin__
            self.redirected = 1
        else:
            sys.stdout = self.tmp_stdout;
            sys.stdin = self.tmp_stdin;
            self.redirected = 0

"""
user question y/n
"""

def ask_user(red, message):
    #pause log tasks
    global pauseLog
    pauseLog = True
    
    #clear stdin
    rlist, _, _ = select.select([sys.stdin], [], [], 0.0)
    if rlist:
        sys.stdin.readline()
    
    #standard in/out temporary redirection for user question/answer
    redirect.InOut(red)
    
    ask = "";
    while ((ask != "Y") and (ask != "N")) :
        print "-------------------------------------------------------------"
        ask = raw_input("%s? [y,n]" % message)
        ask = ask.upper()
        #print "-------------------------------------------------------------"
        print " "

    #restore standard in/out redirection
    redirect.InOut(red)
    
    #resume logging
    pauseLog = False
    
    return ask
        
"""
user question with check action sfp
"""

def ask_user_sfp(red, message, conn, port):
    #pause log tasks
    global pauseLog
    pauseLog = True
    
    #clear stdin
    rlist, _, _ = select.select([sys.stdin], [], [], 0.0)
    if rlist:
        sys.stdin.readline()
    
    #standard in/out temporary redirection for user question/answer
    redirect.InOut(red)
    
    ask = "";
    
    print "-------------------------------------------------------------"
    print "%s [Autodetecting..., Enter to break]" % message
    
    while (ask != "\n") :
        _,result = send_cmd_wrs(conn, 'get_sfp_status', '')

        if port == -1:
            if int(result[1]) == 0:
                break
        elif int(result[1]) == (1 << port):
            break

        #check for user break
        rlist, _, _ = select.select([sys.stdin], [], [], 0.1)
        if rlist:
            ask = sys.stdin.readline()

    print "OK, testing..."

    #restore standard in/out redirection
    redirect.InOut(red)
    
    #resume logging
    pauseLog = False
    
    return ask

"""
user question with check action general button
"""

def ask_user_but(red, message, conn, value):
    #pause log tasks
    global pauseLog
    pauseLog = True
    
    #clear stdin
    rlist, _, _ = select.select([sys.stdin], [], [], 0.0)
    if rlist:
        sys.stdin.readline()
    
    #standard in/out temporary redirection for user question/answer
    redirect.InOut(red)
    
    ask = "";
    
    print "-------------------------------------------------------------"
    print "%s [Autodetecting..., Enter to break]" % message
    
    while (ask != "\n") :
        _,result = send_cmd_wrs(conn, 'get_button', '')

        if int(result[1]) == value:
            break

        #check for user break
        rlist, _, _ = select.select([sys.stdin], [], [], 0.1)
        if rlist:
            ask = sys.stdin.readline()

    print "OK"

    #restore standard in/out redirection
    redirect.InOut(red)
    
    #resume logging
    pauseLog = False
    
    return ask
    
"""
user question custom input
"""

def ask_user_custom(red, message, pattern):
    #pause log tasks
    global pauseLog
    pauseLog = True
    
    #clear stdin
    rlist, _, _ = select.select([sys.stdin], [], [], 0.0)
    if rlist:
        sys.stdin.readline()
        
    #standard in/out temporary redirection for user question/answer
    redirect.InOut(red)
    
    ask = ''
    while (pattern.match(ask) == None):
        print "-------------------------------------------------------------"
        ask = raw_input("%s? " % (message))
        #print "-------------------------------------------------------------"
        print " "

    #restore standard in/out redirection
    redirect.InOut(red)
    
    #resume logging
    pauseLog = False
    
    return ask
        
"""
send command to switch and get answer
"""

def send_cmd_wrs(conn, command, args, timeout=10):
    global pauseLog
    
    #send command
    if pauseLog == False:
        print 'sending command to wrs: ' + command + ' ' + args
    conn.sendall(command + ' ' + args + '\n')

    #check if data available, otherwise wait print waiting
    startTime = time.time()
    while (time.time() - startTime) < timeout:
        rlist, _, _ = select.select([conn], [], [], 1.0)
        if rlist:
            break 
        else:
            if pauseLog == False:
                print '%d seconds elapsed of max %d\r' %((time.time() - startTime), timeout),
                sys.stdout.flush()
    
    #finish
    if pauseLog == False:
        print ''
    
    #receive response       
    data = conn.recv(1024)      
    cmdlist = data.split()
    if pauseLog == False:
        print 'received from wrs: ' + data
    
    #check if response is for given command
    if cmdlist[0] != command:
        raise PtsError('Command error with server, expected: %s but received: %s' % (command, cmdlist[0]))
    
    #check for error:%d response
    try:
        result = cmdlist[1].split(':')
        if result[0] == 'error':
            error = int(result[1])
        else:
            error = 0
    except:
        error = 0
        
    #give some time for logging (log_thread)
    time.sleep(0.2)
    
    return error, cmdlist

"""
thread for logging output of arm+fpga debug port
"""

def uart_log_thread(stop_event):
    usb_tty = find_usb_tty.CttyUSB()
    global pauseLog
    global arm_usb
    lastPrint = ''
    
    while(not stop_event.is_set()):
        try:
            #open port(s)
            usb_port = usb_tty.find_usb_tty(ARM_FPGA_USB_VENDOR_ID, ARM_FPGA_USB_PRODUCT_ID)
            if len(usb_port) >= 1:
                ser = serial.Serial(usb_port[0], 115200, timeout=0)
            if len(usb_port) >= 2:
                ser2 = serial.Serial(usb_port[1], 115200, timeout=0)
                
            #log incoming
            while(not stop_event.is_set()):
                log = ser.readline()
                if pauseLog == False:
                    if log != '' and log.find('softpll:') == -1:
                        arm_usb = usb_port[0]
                        print log,
                
                log = ser2.readline()
                if pauseLog == False:
                    if log != '' and log.find('softpll:') == -1:
                        arm_usb = usb_port[1]
                        print log,
                
                stop_event.wait(0.001)
        
        except Exception as e:
            toPrint = 'UART logging failed due to: %s' % e
            if toPrint != lastPrint:
                print toPrint
            lastPrint = toPrint
        
        finally:
            ser.close()
            ser2.close()
            stop_event.wait(0.001)

"""
thread for logging outputfile
"""

def file_log_thread(file, stop_event):
    pos = 0
    while(not stop_event.is_set()):
        try:
            file = open(file, 'r')
            while(not stop_event.is_set()):
                file.seek(pos)
                log = file.read()
                if log != '':
                    print log,
                    pos += len(log)
                stop_event.wait(0.001)
        
        except Exception as e:
            print 'File logging failed due to: %s' % e
        
        finally:
            file.close()
            stop_event.wait(0.001)

"""
get arm usb port
"""

def get_arm_usb():   
    #open port
    usb_tty = find_usb_tty.CttyUSB()
    usb_port = usb_tty.find_usb_tty(ARM_FPGA_USB_VENDOR_ID, ARM_FPGA_USB_PRODUCT_ID)
    ser = serial.Serial(usb_port[0], 115200, timeout=1)
    
    #find diff between arm/fpga usb
    ser.write('\r\n')
    result = ser.read(512)
    if '#' in result:
        ser.close()
        return usb_port[0]
    else:
        ser.close()
        return usb_port[1]

"""
get fpga usb port
"""

def get_fpga_usb():        
    #open port
    usb_tty = find_usb_tty.CttyUSB()
    usb_port = usb_tty.find_usb_tty(ARM_FPGA_USB_VENDOR_ID, ARM_FPGA_USB_PRODUCT_ID)
    ser = serial.Serial(usb_port[0], 115200, timeout=1)
    
    #find diff between arm/fpga usb
    ser.write('\r\n')
    result = ser.read(512)
    if '#' in result:
        ser.close()
        return usb_port[1]
    else:
        ser.close()
        return usb_port[0]
        
"""
get pts server ip
"""

def socket_init():
    HOST = ''       # Symbolic name meaning all available interfaces
    PORT = 6789     # Arbitrary non-privileged port
    
    print 'Trying to connect to PTSserver on WRS/DUT'
    #open datagram socket
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    
    #Bind socket to local host and port
    count = 0
    while 1:
        try:
            s.bind((HOST, PORT))
            break
        except socket.error as msg:
            time.sleep(1)
            count += 1
            if count == 10:
                break
                raise PtsCritical('Bind failed. Error Code : ' + str(msg[0]) + ' Message ' + msg[1])

    #set timeout
    s.settimeout(5);
    
    #wait for datagram with server address
    try:
        msg, (HOST, port) = s.recvfrom(2)
    except socket.timeout:
        raise PtsCritical('Listen for incoming PTSserver ip timed out')

    #we have connection, close this connection
    print 'Connected with ' + HOST + ':' + str(port)
    s.close()
    
    #open command connection
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.settimeout(10);

    try:
        s.connect((HOST, PORT))
    except socket.error as msg:
        raise PtsCritical('Connect failed. Error Code : ' + str(msg[0]) + ' Message ' + msg[1])
    
    print s.recv(1024) #get welcome message
    return s
            
"""
setup measuring with Pendulum CNT-91
"""

def pendulum_init(meas, channel):
    # Reset device for known state
    meas.reset()
    
    # Set meas mode
    meas.write("MEAS:FREQ? (@%d)" % channel)
    
    # DC coupling
    meas.write("INP:COUP DC")
    meas.write("INP2:COUP DC")
    
    # Set impedance
    meas.write("INP:IMP 50")
    meas.write("INP2:IMP 50")
    
    # Trigger on manual
    meas.write("INP:LEV:AUTO OFF")
    meas.write("INP2:LEV:AUTO OFF")
    
    # Set trigger level (amp +/- 1V (1), -.4 + 3.5V (2))
    meas.write("INP:LEV 0.220")
    meas.write("INP2:LEV 0.875")
    
    # Enable statistics
    meas.write("CALC:AVER:STAT ON")
    
    # Make 100 measurements
    meas.write("CALC:AVER:COUN 100")

"""
reset measuring Pendulum CNT-91
"""

def pendulum_reset(meas):
    # Disable statistics
    meas.write("CALC:AVER:STAT OFF")
    
    # Enable statistics
    meas.write("CALC:AVER:STAT ON")
    
    # Make 100 measurements
    meas.write("CALC:AVER:COUN 100")
   
"""
convert string to int or float
"""
  
def convertString(string):
    # Convert string to int or float
    try:
        #try int
        ret = int(string)
    except ValueError:
        try:
            # if not int try float
            ret = float(string)
        except ValueError:
            # if not int or float, it's empty
            ret = 0
    return ret
    
"""
setup Agilent 33250A
"""

def agilent_init():
    # connect to AWG and test if available 
    usb_tty = find_usb_tty.CttyUSB()
    usb_port = usb_tty.find_usb_tty(AGILENT_USB_VENDOR_ID, AGILENT_USB_PRODUCT_ID)
    
    try:
        print("Opening: %s" % usb_port[0])
        gen = Agilent33250A(device=usb_port[0], bauds=57600)
    except:
        raise PtsError('AWG is not switched on or badly connected, aborting.')
    
    idn = gen.connect()
    if (len(idn) == 0):
        raise PtsError('AWG is not switched on or badly connected, aborting.')
    else:
        print idn
    
    # set sine parameters
    pulse = PulseWaveform()
    pulse.frequency = 10000000
    pulse.amplitude = 1.5
    pulse.dc = 0
    gen.play(pulse)
    #gen.command("PULS:WIDT 0.0000005")
    #gen.command("PULS:TRAN 0.000000005") 

    # enable output
    gen.output = True
    
    return gen
    
"""
framework texts
"""

class framework_Text:
    def __init__(self, TEST_NB, NrOfTests, Subject, Multiple, Critical = False):
        self.startTime = time.time()
        self.TEST_NB = TEST_NB
        self.NrOfTests = NrOfTests
        self.Subject = Subject
        self.Multiple = Multiple
        self.Critical = Critical
        
        print "\n================================================================================"
        print "==> Test%02d start\n" % TEST_NB
        
    def printEnd(self, error):
        print ""
        print "==> End of test%02d" % self.TEST_NB
        print "================================================================================"
        print "Test%02d elapsed time: %.2f seconds\n" % (self.TEST_NB, (time.time()-self.startTime)) 
    
        if(error == [0] * self.NrOfTests):
            print 'The %s %s working fine' % (self.Subject, 'are' if self.Multiple else 'is')
        else:
            if self.Critical:
                raise PtsCritical('An error occured during %s test, check log for details.' % self.Subject)
            else:
                raise PtsError('An error occured during %s test, check log for details.' % self.Subject)
            
"""
print current action
"""

def printAction(text):
    print ''
    print '-' * (len(text) + 8)
    print '    %s' % text
    print '-' * (len(text) + 8)
    print ''
    
"""
ask wrs details
"""

class WRSdetails:
    def __init__(self):
        self.init = True
        self.asked = False
        
    def askDetails(self, red):
        #==================================================
        #ask input
        pattern = re.compile('^[0-9A-Fa-f][02468aceACE]([:][0-9A-Fa-f]{2}){5}$')
        self.MAC1 = ask_user_custom(red, 'What is the MAC1/eth0addr', pattern)
        self.MAC2 = ask_user_custom(red, 'What is the MAC2/wri1addr', pattern)
        
        pattern = re.compile('^[0-9]\.[0-9]$')
        self.Version = ask_user_custom(red, 'What is the SCB version', pattern)
        
        pattern = re.compile('^[0-9]+$')
        self.Serial = ask_user_custom(red, 'What is the Serial', pattern)
        
        pattern = re.compile('^[0-9]+$')
        self.Batch = ask_user_custom(red, 'What is the Batch', pattern)
        
        pattern = re.compile('^[A-Za-z0-9]+$')
        self.FPGA = ask_user_custom(red, 'What is the FPGA', pattern)
        
        pattern = re.compile('^[A-Za-z0-9 ]+$')
        self.Manufacturer = ask_user_custom(red, 'What is the Manufacturer', pattern)
        
        self.asked = True
        
    def getAsked(self):
        return self.asked
    
    def setHwinfo(self, default_directory):
        manufacturer_file = '%s/hwinfo/source/manufacturer' % default_directory
        
        #==================================================
        #prepare for manufacturer replacement
        with open(manufacturer_file, 'w') as f:
            f.write(self.Manufacturer)
        
        #==================================================
        #create base hwinfo
        printAction('Create sdb-for-dataflash base image')
        subprocess.check_output(['%s/hwinfo/gensdbfs' % default_directory, \
        '%s/hwinfo/source/' % default_directory, '%s/hwinfo/sdb-for-dataflash' % default_directory])
        
        #==================================================
        #edit hwinfo file
        printAction('Editing hwinfo')
        print 'Editing HWinfo file'
        PathHWinfo = subprocess.check_output(['%s/hwinfo/wrs_hwinfo' % default_directory, \
        '-m1', self.MAC1, '-m2', self.MAC2, '-v', self.Version, '-n', self.Serial, '-b', self.Batch, '-f', self.FPGA])
        PathHWinfo = PathHWinfo.strip()
        print 'HWinfo file saved to ' + PathHWinfo
        
        #==================================================
        #copy to nfs update dir
        print 'Copy hwinfo to nfs folder'
        subprocess.check_output(['cp', PathHWinfo, '/tftpboot/rootfs/wr/bin/hwinfo'])
        
    def getDetails(self):
        return self.MAC1, self.MAC2, self.Version, self.Serial, self.Batch, self.FPGA, self.Manufacturer
    
#global
WRS_details = WRSdetails()

def setWRSdetails(value):
    global WRS_details
    WRS_details = value
    
def getWRSdetails():
    return WRS_details