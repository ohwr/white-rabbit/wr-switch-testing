#!   /usr/bin/env   python
#    coding: utf8

# Copyright CERN, 2017
# Author: INCAA Computers
# License: GPL v2 or later.
# Website: http://www.ohwr.org

# Import toolbox
from utilFunctions import *

def main (default_directory='.'):

    #constants declaration
    TEST_NB = 2
    NrOfTests = 16
    Subject = 'hwinfo'
    Multiple = False
    Critical = True
    
    red = redirect(0)
    thread_stop = threading.Event()
    parts = ['eth0.ethaddr', 'wri1.ethaddr', 'scb_version', 'manufacturer', 'hw_info']
    values = ['']
    
    #==================================================
    #print start text
    frameworkText = framework_Text(TEST_NB, NrOfTests, Subject, Multiple, Critical)
    
    try:
        #initialize error variable
        error = [0] * NrOfTests
        
        #check if root
        if os.geteuid() != 0:
            print 'Sorry, I must be run as root'
            raise PtsError('Sorry, I must be run as root')
            
        #open socket
        conn = socket_init()
        
        #use debug port for logging for rest
        threading.Thread(target = uart_log_thread, args = (thread_stop,)).start()
        
        #==================================================
        #hwinfo editing
        WRS_details = getWRSdetails()
        if not WRS_details.getAsked():
            WRS_details.askDetails(red)
            
        WRS_details.setHwinfo(default_directory)
        MAC1, MAC2, Version, Serial, Batch, FPGA, Manufacturer = WRS_details.getDetails()
        print WRS_details.getDetails()
        
        #==================================================
        #install in switch
        printAction('Install hwinfo into switch')
        print 'Enable hwinfo write device'
        cmd = 'test -c /dev/mtd5 || mknod /dev/mtd5 c 90 10'
        error[0],_ = send_cmd_wrs(conn, 'exec_cmd_shell', cmd)
        with open(cmd_output, 'r') as f:
            print f.read()
            
        print 'Erase current hwinfo'
        cmd = 'flash_erase /dev/mtd5 0 0'
        error[1],_ = send_cmd_wrs(conn, 'exec_cmd_shell', cmd)
        with open(cmd_output, 'r') as f:
            print f.read()
            
        print 'Set current hwinfo'
        cmd = 'cat /wr/bin/hwinfo > /dev/mtd5'
        error[2],_ = send_cmd_wrs(conn, 'exec_cmd_shell', cmd)
            
        print 'Remove hwinfo write device'
        cmd = 'rm /dev/mtd5'
        error[3],_ = send_cmd_wrs(conn, 'exec_cmd_shell', cmd)
        with open(cmd_output, 'r') as f:
            print f.read()
    
        #==================================================
        #check main
        printAction('Checking written hwinfo file')
        print 'Read back hwinfo'
        cmd = '/wr/bin/sdb-read /dev/mtd5ro'
        error[4],_ = send_cmd_wrs(conn, 'exec_cmd_shell', cmd)
        
        with open(cmd_output, 'r') as f:
            output = f.read()
        print 'SDB main'
        print output
        for i in range(len(parts)):
            if output.find(parts[i]) == -1:
                print '%s not found' % parts[i]
                error[5] += 1
        
        #==================================================
        #check hw_info
        print 'Read back hw_info'
        cmd = '/wr/bin/sdb-read /dev/mtd5ro hw_info'
        error[6],_ = send_cmd_wrs(conn, 'exec_cmd_shell', cmd)
        
        with open(cmd_output, 'r') as f:
            output = f.read()
            
        print 'hw_info'
        print output
        if output.find('scb_serial: %s' % Serial) == -1:
            print 'scb_serial is not matching'
            error[7] += 1
        if output.find('scb_batch: %s' % Batch) == -1:
            print 'scb_batch is not matching'
            error[7] += 1
        if output.find('fpga_type: %s' % FPGA) == -1:
            print 'fpga_type is not matching'
            error[7] += 1

        #==================================================
        #check others
        print 'Read back extra'
        values = [MAC1, MAC2, Version, Manufacturer]
        for i in range(len(values)):
            cmd = '/wr/bin/sdb-read /dev/mtd5ro %s' % parts[i]
            error[8+i],_ = send_cmd_wrs(conn, 'exec_cmd_shell', cmd)
            
            with open(cmd_output, 'r') as f:
                output = f.read()
                
            print '%s: %s' % (parts[i], output)
            if output.strip() != values[i]:
                print '%s is not matching' % parts[i]
                error[12+i] += 1

    #==================================================
    except subprocess.CalledProcessError as e:
        raise PtsError("Test Failed: command '%s' gave result %d with description '%s'" 
        % (e.cmd, e.returncode, e.output))
        
    except Exception as e:
        raise PtsError("Test Failed: %s" % e)
        
    finally:
        if 'thread_stop' in locals():
            thread_stop.set()
        if 'conn' in locals():
            conn.close()
    
    #==================================================
    #process result
    if(error[0:4] != [0] * 4):
        print 'Error occurred during writing hwinfo to DUT'
    if error[4] != 0:
        print 'Reading of the written hwinfo failed'
    if error[5] != 0:
        print '%d parts of hwinfo not matching' % error[5]
    if error[6] != 0:
        print 'Reading of hwinfo part failed'
    if error[7] != 0:
        print '%d parts of hwinfo part not matchine' % error[7]
    for i in range(4):
        if error[8+i] != 0:
            print 'Reading of %s part failed' % parts[i]
        if error[12+i] != 0:
            print 'Value of %s part not matching' % parts[i]
    
    #print end text
    frameworkText.printEnd(error)
        
if __name__ == '__main__' :
    main()
