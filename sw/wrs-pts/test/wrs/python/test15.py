#!   /usr/bin/env   python
#    coding: utf8

# Copyright CERN, 2017
# Author: INCAA Computers
# License: GPL v2 or later.
# Website: http://www.ohwr.org

# Import toolbox
from utilFunctions import *

def main (default_directory='.'):

    #constants declaration
    TEST_NB = 15
    NrOfTests = 4
    Subject = 'USB ports + RS232 port'
    Multiple = True
    
    usb_tty = find_usb_tty.CttyUSB()
        
    #==================================================
    #print start text
    frameworkText = framework_Text(TEST_NB, NrOfTests, Subject, Multiple)
    
    try:
        #initialize error variable
        error = [0] * NrOfTests
        
        #open socket
        conn = socket_init()

        #==================================================
        #test management usb port
        printAction('Testing management USB port')
        
        usb_port = usb_tty.find_usb_tty(MGMT_USB_VENDOR_ID, MGMT_USB_PRODUCT_ID)
        ser = serial.Serial(usb_port[0], 115200, timeout=1)
        print 'Opening: %s' % usb_port[0]
        
        #flush
        ser.write('ls\n');
        while 1:
            try:
                read = ser.read(4096)
                if read == '':
                    break
            except:
                break
        
        #test
        print 'Write CAFEBABE, expecting DEADBEEF'
        ser.write('/wr/bin/PTS_com_test CAFEBABE\n')
        time.sleep(1)
        print 'Echo back from port: %s' % ser.readline()
        result = ser.readline()
        print 'Result: %s' % result
        
        if result.strip() != 'DEADBEEF':
            error[0] = 1
        
        #close port
        ser.close()
        
        #==================================================
        #test arm usb port
        printAction('Testing ARM usb port')
        
        port = get_arm_usb()
        ser = serial.Serial(port, 115200, timeout=1)
        print 'Opening: %s' % port
        
        #flush
        ser.write('ls\n');
        while 1:
            try:
                read = ser.read(4096)
                if read == '':
                    break
            except:
                break
        
        #test
        print 'Write CAFEBABE, expecting DEADBEEF'
        ser.write('/wr/bin/PTS_com_test CAFEBABE\n')
        time.sleep(1)
        print 'Echo back from port: %s' % ser.readline()
        result = ser.readline()
        print 'Result: %s' % result
        
        if result.strip() != 'DEADBEEF':
            error[1] = 1
            
        #close port
        ser.close()
        
        #==================================================
        #test fpga usb port
        printAction('Testing FPGA usb port')
        
        port = get_fpga_usb()
        ser = serial.Serial(port, 115200, timeout=1)
        print 'Opening: %s' % port
        
        #flush
        ser.write('\r')
        while 1:
            try:
                read = ser.read(4096)
                if read == '':
                    break
            except:
                break
        
        #test
        print 'Write CAFEBABE, expecting DEADBEEF'
        ser.write('C')
        time.sleep(0.1)
        ser.write('A')
        time.sleep(0.1)
        ser.write('F')
        time.sleep(0.1)
        ser.write('E')
        time.sleep(0.1)
        ser.write('B')
        time.sleep(0.1)
        ser.write('A')
        time.sleep(0.1)
        ser.write('B')
        time.sleep(0.1)
        ser.write('E')
        time.sleep(0.1)
        ser.write('\r')
        time.sleep(1)

        print 'Echo back from port: %s' % ser.readline() #read back command
        print 'FPGA received command: %s' % ser.readline() #fpga received command
        
        result = ser.read(512) #result
        print 'Result: %s' % result
        if result.strip() != 'DEADBEEF':
            error[2] = 1
            
        #close
        ser.close()
        
        #==================================================
        #test arm rs232 port
        printAction('Testing RS232 port')
        
        #start responder for rs232 port
        send_cmd_wrs(conn, 'start_ttyS2_handler', '')
            
        #open port
        usb_port = usb_tty.find_usb_tty(RS232_USB_VENDOR_ID, RS232_USB_PRODUCT_ID)
        ser = serial.Serial(usb_port[0], 115200, timeout=1)
        print 'Opening: %s' % usb_port[0]
        
        #flushing
        ser.write('\n')
        time.sleep(0.1)
        ser.readline()
        
        #test
        print 'Writing CAFEBABE, expecting cafebabe'
        ser.write('C')
        time.sleep(0.1)
        ser.write('A')
        time.sleep(0.1)
        ser.write('F')
        time.sleep(0.1)
        ser.write('E')
        time.sleep(0.1)
        ser.write('B')
        time.sleep(0.1)
        ser.write('A')
        time.sleep(0.1)
        ser.write('B')
        time.sleep(0.1)
        ser.write('E')
        time.sleep(0.1)
        time.sleep(1)
        result = ser.read(8)
        print 'Result: %s' % result

        if result.strip() != 'cafebabe':
            error[3] = 1

        #stop responder for rs232 port
        send_cmd_wrs(conn, 'stop_ttyS2_handler', '')
        
    #==================================================
    except Exception as e:
        raise PtsError("Test Failed: %s" % e)
        
    finally:
        if 'ser' in locals():
            ser.close()
        if 'conn' in locals():
            conn.close()
            
    #==================================================
    #process result
    if(error[0] != 0):
        print("Management USB port is not working OK")
    if(error[1] != 0):
        print("ARM USB port is not working OK")
    if(error[2] != 0):
        print("FPGA USB port is not working OK")
    if(error[3] != 0):
        print("ARM RS232 port is not working OK")
    
    #print end text
    frameworkText.printEnd(error)
        
if __name__ == '__main__' :
    main()