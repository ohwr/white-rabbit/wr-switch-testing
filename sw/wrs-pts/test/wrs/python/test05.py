#!   /usr/bin/env   python
#    coding: utf8

# Copyright CERN, 2017
# Author: INCAA Computers
# License: GPL v2 or later.
# Website: http://www.ohwr.org

# Import toolbox
from utilFunctions import *

def main (default_directory='.'):

    #constants declaration
    TEST_NB = 5
    NrOfTests = 4
    Subject = 'FANs'
    Multiple = True
    
    red = redirect(0)
    thread_stop = threading.Event()
        
    #==================================================
    #print start text
    frameworkText = framework_Text(TEST_NB, NrOfTests, Subject, Multiple)
    
    try:
        #initialize error variable
        error = [0] * NrOfTests
        
        #open socket
        conn = socket_init()
        
        #use debug port for logging for rest
        threading.Thread(target = uart_log_thread, args = (thread_stop,)).start()
      
        #==================================================
        #test on
        printAction('Fans put on')
        send_cmd_wrs(conn, 'set_fan_speed', '3 1')

        if (ask_user(red, 'Are both fans on') == "N"):
            print 'One or both fans are not connected/soldered correctly'
            error[0] = 1
            
        #==================================================
        #test off
        printAction('Fans put off')
        send_cmd_wrs(conn, 'set_fan_speed', '3 0')

        if (ask_user(red, 'Are both fans off') == "N"):
            print 'One or both fans are shortcircuit'
            error[1] = 1
        
        #==================================================
        #test fan1 on
        printAction('Fan1 put on')
        send_cmd_wrs(conn, 'set_fan_speed', '1 1')

        if (ask_user(red, 'Do you hear a fan turning') == "N"):
            print 'Fan1 is not switching on, caution for overheating!'
            error[2] = 1
 
        #==================================================
        #test fan2 on
        printAction('Fan1 put off, Fan2 put on')
        send_cmd_wrs(conn, 'set_fan_speed', '1 0')
        send_cmd_wrs(conn, 'set_fan_speed', '2 1')

        if (ask_user(red, 'Do you hear a fan turning') == "N"):
            print 'Fan2 is not switching on, caution for overheating!'
            error[3] = 1

        #set both fans to full speed
        printAction('Fans put on')
        send_cmd_wrs(conn, 'set_fan_speed', '3 1')

    #==================================================
    except Exception as e:
        raise PtsError("Test Failed: %s" % e)
        
    finally:
        if 'thread_stop' in locals():
            thread_stop.set()
        if 'conn' in locals():
            conn.close()
            
    #==================================================
    #process result
    if(error[0] != 0):
        print 'One or both FANs not switching ON. Check for bad soldering.'
    if(error[1] != 0):
        print 'One or both FANs not switching OFF. Check for short-cut.'
    if(error[2] != 0):
        print 'CAUTION! FAN 1 is not switching ON. Risk of overheating!'
    if(error[3] != 0):
        print 'CAUTION! FAN 2 is not switching ON. Risk of overheating!'

    #print end text
    frameworkText.printEnd(error)        

if __name__ == '__main__' :
    main()