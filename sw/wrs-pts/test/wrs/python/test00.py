#!   /usr/bin/env   python
#    coding: utf8

# Copyright CERN, 2017
# Author: INCAA Computers
# License: GPL v2 or later.
# Website: http://www.ohwr.org

# Import toolbox
from utilFunctions import *

def main (default_directory='.'):

    #constants declaration
    TEST_NB = 0
    NrOfTests = 2
    Subject = 'initial flashing'
    Multiple = False
    Critical = True
    
    red = redirect(0)
    thread_stop = threading.Event()
    usb_tty = find_usb_tty.CttyUSB()
    srcPath = '%s/../firmware' % default_directory
    srcFile = 'wrs-firmware-pts.tar'
    dstPath = '/tftpboot'
    dstFile = 'wrs-firmware.tar'
    logfile = '/tmp/pexpect.log'
    timeout_serial = 300
    
    #==================================================
    #print start text
    frameworkText = framework_Text(TEST_NB, NrOfTests, Subject, Multiple, Critical)
    
    try:
        #initialize error variable
        error = [0] * NrOfTests
        
        #check firmware file on right path
        if not (os.path.isfile('%s/%s' % (srcPath, srcFile))):
            raise PtsCritical('%s/%s not found' % (srcPath, srcFile))
            
        #check if root
        if os.geteuid() != 0:
            print 'Sorry, I must be run as root'
            raise PtsError('Sorry, I must be run as root')

        #==================================================
        #ask input for wrs details
        WRS_details = getWRSdetails()
        WRS_details.askDetails(red)
        setWRSdetails(WRS_details)
        
        #==================================================
        #copy wrs-firmware.tar to tftpboot
        printAction('Copy %s/%s into %s/%s' % (srcPath, srcFile, dstPath, dstFile))
        print subprocess.check_output(['cp', '%s/%s' % (srcPath, srcFile), '%s/%s' % (dstPath, dstFile)])
        
        #==================================================
        printAction('Asking user to boot DUT into bootloader')
        #standard in/out temporary redirection for user question/answer
        redirect.InOut(red)
        
        #ask user to enable bootloader
        print '     Waiting for at91sam SAMBA bootloader on usb.'
        print '     Please check the Management USB cable is connected and keep '
        print '     pressed the Flash button while resetting/powering the switch.'
        
        #wait for action user 
        while 1:
            bootl_usb = usb_tty.find_usb_tty(BOOTL_USB_VENDOR_ID, BOOTL_USB_PRODUCT_ID)
            if bootl_usb:
                if bootl_usb[0]:
                    break
            time.sleep(1)
        
        raw_input('Ok. Please release the flash button and press any key to continue')
        print 'Ok, busy...'
        time.sleep(2)
        
        #restore standard in/out redirection
        redirect.InOut(red)

        #==================================================
        #open debug port for logging
        threading.Thread(target = uart_log_thread, args = (thread_stop,)).start()
        
        #start flashing
        output = subprocess.check_output(['%s/flash-wrs' % default_directory, '-e', '%s/%s' % (srcPath, srcFile)])
        printAction('Flash-wrs script output:')
        print output
        
        #check result
        printAction('Check result')
        if output.find('\nDone\n') == -1:
            print 'Did not find Done result'
            error[0] = 1
        
        #==================================================
        #stop log thread
        thread_stop.set()
        time.sleep(1)
        
        #replace or create logfile, then start log thread
        log = open(logfile, 'w+')
        thread_stop.clear()
        threading.Thread(target = file_log_thread, args = (logfile, thread_stop)).start()
        
        #get serial port
        printAction('opening pexpect on ' + arm_usb())
        ser = serial.Serial(arm_usb(), 115200, timeout=timeout_serial)
        
        #open pexpect task
        reader = fdspawn(ser, 'wb')
        reader.logfile = log
        reader.timeout = timeout_serial

        #==================================================
        #wait for finalizing
        printAction('Waiting for finalizing')
        try:
            reader.expect('\nwrs[\x21-\x7E]*#')
        except Exception as e:
            print 'Error occured while waiting on finalization: %s' % e
            error[1] = 1
                
    #==================================================    
    except subprocess.CalledProcessError as e:
        raise PtsError("Test Failed: command '%s' gave result %d with description '%s'" 
        % (e.cmd, e.returncode, e.output))
        
    except Exception as e:
        raise PtsError("Test Failed: %s" % e)
        
    finally:
        if 'thread_stop' in locals():
            thread_stop.set()
        if 'reader' in locals():
            reader.close()
        if 'log' in locals():
            log.close()
    
    #==================================================
    #process result
    if (error[0] != 0):
        print 'Flashing of the WRS failed'
    if (error[1] != 0):
        print 'Timed out waiting for finalizing'
    
    #print end text
    frameworkText.printEnd(error)

if __name__ == '__main__' :
    main()