#!   /usr/bin/env   python
#    coding: utf8

# Copyright CERN, 2017
# Author: INCAA Computers
# License: GPL v2 or later.
# Website: http://www.ohwr.org

# Import toolbox
from utilFunctions import *

def main (default_directory='.'):

    #constants declaration
    TEST_NB = 13
    NrOfTests = 5
    Subject = 'onboard clocks'
    Multiple = True
    
    thread_stop = threading.Event()
    usb_tmc = find_usb_tmc.CtmcUSB()
    freq = [62.5, 25.0, 15.625, 31.25, 62.5]
    margin = [0.1, 0.1, 0.1, 0.1, 0.1]
    regs = [0x7B000, 0x7B008, 0x7B00C, 0x7B010, 0x7B014]
    names = ['DMTD_CLK', 'FPGA_MAIN_CLK', 'AUX_CLK', 'SERDES_CLK', 'REF_CLK']
    
    #==================================================
    #print start text
    frameworkText = framework_Text(TEST_NB, NrOfTests, Subject, Multiple)
    
    try:
        #initialize error variable
        error = [0] * NrOfTests
        
        #check if root
        if os.geteuid() != 0:
            print 'Sorry, I must be run as root'
            raise PtsError('Sorry, I must be run as root')
            
        #open socket
        conn = socket_init()
        
        #use debug port for logging for rest
        threading.Thread(target = uart_log_thread, args = (thread_stop,)).start()

        #==================================================
        #init CNT-91
        printAction('Initialize CNT-91 counter')
        usb_port = usb_tmc.find_usb_tmc(CNT91_USB_VENDOR_ID, CNT91_USB_PRODUCT_ID)
        try:
            meas = PendulumCNT91(usb_port[0])
        except OSError as e:
            print e
            raise PtsError('CNT-91 is not switched on or badly connected, aborting.')

        #setup CNT-91
        pendulum_init(meas, 1)
        
        #==================================================
        #temperorary set serdes clk to 31.25 mhz, divider 16, afterwards back to 1
        printAction('Set serdes clk output to measureable frequency')
        send_cmd_wrs(conn, 'set_ad9516', '5 16 0')
                
        #==================================================
        for i in range(5):
            printAction('Measuring %s with expected freq of %.3fMHz' % (names[i], freq[i]))
            
            _,result = send_cmd_wrs(conn, 'read_reg_fpga', '%d' % regs[i])
            value = 3 * int(result[1])
            value = (value/1000000.0) #mhz
            
            print 'Measured freq on %s is %.3fMHz' % (names[i], value)
            if (value > (freq[i] + margin[i]) or (value < (freq[i] - margin[i]))):
                error[3] += 1

        #==================================================
        #temperorary set serdes clk to 31.25 mhz, divider 16, afterwards back to 1
        printAction('Set serdes clk output back to normal frequency')
        send_cmd_wrs(conn, 'set_ad9516', '5 1 0')
     
    #==================================================     
    except Exception as e:
        raise PtsError("Test Failed: %s" % e)
        
    finally:
        if 'thread_stop' in locals():
            thread_stop.set()
        if 'conn' in locals():
            conn.close()
    
    #==================================================
    #process result
    for i in range(5):
        if(error[i] != 0):
            print 'Measured frequency of %s did not comply with expected frequency' % names[i]
    
    #print end text
    frameworkText.printEnd(error)
        
if __name__ == '__main__' :
    main()