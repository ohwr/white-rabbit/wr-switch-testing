#!   /usr/bin/env   python
#    coding: utf8

# Copyright CERN, 2017
# Author: INCAA Computers
# License: GPL v2 or later.
# Website: http://www.ohwr.org

# Import toolbox
from utilFunctions import *

def main (default_directory='.'):

    #constants declaration
    TEST_NB = 12
    NrOfTests = 6
    Subject = 'AD9516'
    Multiple = False
    
    thread_stop = threading.Event()
    usb_tmc = find_usb_tmc.CtmcUSB()
    freq = [250.0, 31.25, 15.625]
    margin = [1.0, 1.0, 1.0]
    parms = [2, 16, 32]
    refsel = [0, 1, 0]
    sync = [1, 0, 1]
    results = [1, 0, 1]
    
    #==================================================
    #print start text
    frameworkText = framework_Text(TEST_NB, NrOfTests, Subject, Multiple)
    
    try:
        #initialize error variable
        error = [0] * NrOfTests
        
        #check if root
        if os.geteuid() != 0:
            print 'Sorry, I must be run as root'
            raise PtsError('Sorry, I must be run as root')
            
        #open socket
        conn = socket_init()
        
        #use debug port for logging for rest
        threading.Thread(target = uart_log_thread, args = (thread_stop,)).start()

        #==================================================
        #init CNT-91
        printAction('Initialize CNT-91 counter')
        usb_port = usb_tmc.find_usb_tmc(CNT91_USB_VENDOR_ID, CNT91_USB_PRODUCT_ID)
        try:
            meas = PendulumCNT91(usb_port[0])
        except OSError as e:
            print e
            raise PtsError('CNT-91 is not switched on or badly connected, aborting.')

        #setup CNT091
        pendulum_init(meas, 1)
        
        #==================================================
        #init AD9516 for test
        printAction('Initializing AD9516 for test')
        result,_ = send_cmd_wrs(conn, 'init_ad9516', '')
        
        if (result != 0):
            print 'Initialization of AD9516 failed'
            error[5] = 1
        
        #==================================================
        #test clk1 (out9)
        for i in range(3):
            printAction('Test %d: clock at %.3fMHz' % (i+1, freq[i]))
            
            #set clk1
            send_cmd_wrs(conn, 'set_ad9516', '9 %d 0' % parms[i])
                
            # restart CNT-91
            pendulum_reset(meas)
            
            # read result and check
            time.sleep(2)
            meas.write("CALC:AVER:COUN:CURR?")
            n = meas.read()
            if (convertString(n) != 100):
                error[i] += 1
                print "CLK1 is not switching on"
            
            # read data and store
            meas.write("CALC:DATA?")
            res = meas.read()
            value = convertString(res)
            if (value != 0):
                value = (value/1000000.0) #mhz
                print 'Measured freq on CLK1 is %.3fMHz' % value
                if (value > (freq[i] + margin[i]) or (value < (freq[i] - margin[i]))):
                    error[i] += 1
                    
        #==================================================
        #test pll signals refsel/sync/stat/lock
        status = 0x7F004
        for i in range(3):
            printAction('Test with sync: %d and refsel: %d' % (sync[i], refsel[i]))
            
            #set ref_sel
            send_cmd_wrs(conn, 'write_gpio_control', '20 %d' % refsel[i])
                
            #set sync_n
            send_cmd_wrs(conn, 'write_gpio_control', '21 %d' % sync[i])

            #read status
            _,output = send_cmd_wrs(conn, 'read_reg_fpga', '%d' % status)
            result = int(output[1])
            print 'Stat(sync_pin): %d and Lock(refsel_pin): %d' % \
            ((result & 1<<20) != 0, (result & 1<<21) != 0)
            for y in range(2):
                if (result & 1<<(20+y)) == results[i]:
                    error[3+y] += 1

    #==================================================
    except Exception as e:
        raise PtsError("Test Failed: %s" % e)
        
    finally:
        if 'thread_stop' in locals():
            thread_stop.set()
        if 'conn' in locals():
            conn.close()
        if 'meas' in locals():
            meas.close()
    
    #==================================================
    #process result
    if(error[0:3] != [0] * 3):
        print 'Clock output of AD9516 was not programmable or soldered correctly'
    if(error[3] != 0):
        print 'Stat and/or sync pin(s) connected to AD9516 where not OK'
    if(error[4] != 0):
        print 'Lock and/or refsel pin(s) connected to AD9516 where not OK'
    if(error[5] != 0):
        print 'Initialization of AD9516 for test failed'
    
    #print end text
    frameworkText.printEnd(error)
        
if __name__ == '__main__' :
    main()