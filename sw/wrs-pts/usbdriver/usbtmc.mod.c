#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
 .name = KBUILD_MODNAME,
 .init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
 .exit = cleanup_module,
#endif
 .arch = MODULE_ARCH_INIT,
};

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x4d5503c4, "module_layout" },
	{ 0x11eb03aa, "usb_deregister" },
	{ 0x7485e15e, "unregister_chrdev_region" },
	{ 0xc6c5a6, "usb_register_driver" },
	{ 0x29537c9e, "alloc_chrdev_region" },
	{ 0x983743f, "usb_reset_configuration" },
	{ 0xd0d8621b, "strlen" },
	{ 0x3c2c5af5, "sprintf" },
	{ 0x2f287f0d, "copy_to_user" },
	{ 0x362ef408, "_copy_from_user" },
	{ 0x34eba3df, "usb_bulk_msg" },
	{ 0x30a1a8ae, "usb_control_msg" },
	{ 0xc11c504f, "dev_set_drvdata" },
	{ 0x2753ef5c, "cdev_add" },
	{ 0x26a0a5cf, "cdev_init" },
	{ 0x50eedeb8, "printk" },
	{ 0x76fe116b, "kmem_cache_alloc_trace" },
	{ 0x887f972d, "kmalloc_caches" },
	{ 0x7ac957ab, "usb_get_dev" },
	{ 0x621a18bd, "cdev_del" },
	{ 0x4be141b4, "dev_get_drvdata" },
	{ 0x37a0cba, "kfree" },
	{ 0xb4390f9a, "mcount" },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=";


MODULE_INFO(srcversion, "F441DD0E780FE4FB5D2A150");
