#!/bin/sh

# License: GPL v2 or later.
# Website: http://www.ohwr.org

LOGDIR=./log_wrs

mkdir -p $LOGDIR
sudo rm -fr $LOGDIR/pts*

serial=$1
if [ x$1 = x"" ]; then
	echo -n "Please scan CERN serial number bar-code, then press [ENTER]: "
        read serial
fi

if [ x$serial = x"" ]; then
	serial=0000
fi

extra_serial=$2
if [ x$2 = x"" ]; then
	echo -n "If needed input extra serial number and press [ENTER] OR just press [ENTER]: "
        read extra_serial
fi

if [ x$extra_serial = x"" ]; then
	extra_serial=0000
fi

echo " "

nb_test_limit=2
nb_test=1

while [ "$nb_test" -le "$nb_test_limit" ]
do
    echo "--------------------------------------------------------------"
    echo "Test series run $nb_test out of $nb_test_limit"
    echo " "

    sudo ./pts.py -b WRS -s $serial -e $extra_serial -t./test/wrs/python -l $LOGDIR 00 01 02 03 04 05 06 07 08 09 10 11 12 13 14 15 16 17

    if [ "$nb_test" != "$nb_test_limit" ]
    then
        echo " "
        echo -n "Do you want to run the test series again [y,n]? "
        read reply
        if [ "$reply" != "y" ]
        then
            break
        fi
    fi
    nb_test=$(($nb_test+1))
done

echo "--------------------------------------------------------------"
echo " "
echo -n "End of the test"
echo " "

