#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <arpa/inet.h>
#include <linux/if_packet.h>
#include <linux/if_ether.h>
#include <linux/if_arp.h>
#include <linux/net_tstamp.h>
#include <linux/errqueue.h>
#include <linux/sockios.h>
#include <sys/time.h>
#include <asm/types.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/time.h>
#include <asm/socket.h>
#include <signal.h>
#include "ptpd_netif.h"
#include "hal_client.h"

#define NUMBER_PORTS        18
#define IFACE_NAME_LENGTH   16
#define UUID_LENGTH         6
#define ETHER_MTU           1518

typedef struct
{
    int repeat;
    uint64_t start_tics;
    uint64_t timeout;
} timeout_t;  //

typedef struct timeval timeval_t;

PACKED struct etherframe {
    struct ethhdr ether;
    char data[ETHER_MTU];
};

const mac_addr_t MULTICAST_ADDR = {0x01, 0x1b, 0x19, 0 , 0, 0};
const mac_addr_t UNICAST_ADDR   = {0x01, 0x1b, 0x19 , 0  , 0, 0};
const mac_addr_t BCAST_ADDR   = {0xFF, 0xFF, 0xFF , 0xFF , 0xFF, 0xFF};
const mac_addr_t CPU_ADDR   = {0, 0, 0 , 0 , 0, 0};

typedef struct {
    int         family;
    mac_addr_t  mac;
    mac_addr_t  mac_dest;
    ipv4_addr_t ip;
    uint16_t    ethertype;
    uint16_t    physical_port;
    char        if_name[IFACE_NAME_LEN];
} sockaddr_t;

typedef struct{
    int fd;
    sockaddr_t bind_addr;
    mac_addr_t local_mac;
    int if_index;
    uint32_t clock_period;
    uint32_t phase_transition;
    uint32_t dmtd_phase;
    int dmtd_phase_valid;
    timeout_t dmtd_update_tmo;
} socket_t;

typedef struct {
    socket_t        *sock;
    sockaddr_t      multicastAddr;
    sockaddr_t      unicastAddr;
    sockaddr_t      selfAddr;
    char            port_uuid_field[UUID_LENGTH];
    char            ifaceName[IFACE_NAME_LEN];
    uint16_t        portNumber;
    uint8_t         linkUP;
} net_t;



