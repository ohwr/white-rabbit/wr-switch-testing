\section{Synchronization performance}
\label{chap:synch-performance}

The purpose of these tests if to verify the performance of WR synchronization on
the WR Switch under test.

\paragraph{Hardware requirements}
\vspace{12pt}
\begin{itemize}
  \item 2x WR Switch under test (calibrated according to WR calibration document \cite{WRCalib})
  \item 3x Simple PCIe FMC Carrier (SPEC) with FMC DIO 5-channel module
    (calibrated according to WR calibration document \cite{WRCalib})
  \item 1x 4-channel oscilloscope (e.g. 200MHz, 2GS/s)
  \item 1x clock reference producing 10MHz and 1-PPS signal (e.g. GPSDO or
    Cesium atomic clock)
  \item 1x Link breaker device (to be built from fiber switches)
  \item 1x Networking performance tester (the same used in forwarding tests,
    sections \ref{chap:fwd-functional}, \ref{chap:fwd-performance})
\end{itemize}

\paragraph{Default Switch Configuration}

\begin{table}[ht!]
  \begin{tabular}{l  l l}
    PPSI & port 0:      & slave\\
         & other ports: & master\\
  \end{tabular}
\end{table}

\newpage %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Long term synchronization}

These tests verify if the WR Switch under test is able to perform a long term
synchronization without becoming unlocked and without any time jumps.\\

\noindent Tests should be run over a period of several days/weeks.

\subsubsection{Master and Slave mode}

\paragraph{Description}
The test verifies whether the switch will be constantly synchronized to another
WR Master over long period of time and if it will provide stable timing to other
WR Slaves in the hierarchy.

\paragraph{Test setup}
\vspace{12pt}
\begin{table}[ht!]
  \begin{tabular}{r l l}
    WR Switch: & {\bf port 0}    & -- SPEC running WR PTP Core in \emph{WR Master} mode\\
    WR Switch: & {\bf port 2}    & -- SPEC running WR PTP Core in \emph{WR Slave} mode\\
    WR Switch: & {\bf port 18}   & -- SPEC running WR PTP Core in \emph{WR Slave} mode\\
  Master SPEC: & {\bf 1-PPS out} & -- channel 1 of the oscilloscope\\
    WR Switch: & {\bf 1-PPS out} & -- channel 2 of the oscilloscope\\
  Slave SPECs: & {\bf 1-PPS out} & -- channels 3 and 4 of the oscilloscope\\
  \end{tabular}
\end{table}

\paragraph{Oscilloscope setup}
\vspace{12pt}
\begin{table}[ht!]
  \begin{tabular}{l l}
    {\bf channels 1,3,4} & 200mV/div, offset 1.25V, DC 50 Ohm\\
         {\bf channel 2} & 200mV/div, offset 800mV, DC 50 Ohm\\
       {\bf timescale} & 1ns/div\\
 {\bf persistent mode} & ON\\
  \end{tabular}
\end{table}

\paragraph{Test passed if}
\begin{itemize*}
  \item Oscilloscope in persistent mode shows stable 1-PPS relation between all
    devices - i.e. each of the device produces a single, blurred trace on the
    screen
  \item \emph{wr\_mon} tool on WR Switch reports through the whole test period:
    \begin{itemize}
      \item \emph{lock} always 1
      \item \emph{servo state} always \emph{TRACK\_PHASE}
      \item \emph{mu} changes without any sudden, few-ns jumps
      \item \emph{cko} always in a range -50ps; +50ps
    \end{itemize}
\end{itemize*}


\newpage %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{GrandMaster mode}

\paragraph{Description}
The test verifies whether the switch under test is able to work stable in a
GrandMaster mode being locked to an external clock (e.g. GPSDO or Cesium)

\paragraph{Test setup}
\vspace{12pt}
\begin{table}[ht!]
  \begin{tabular}{r l l}
    WR Switch: & {\bf port 2}    & -- SPEC running WR PTP Core in \emph{WR Slave} mode\\
    WR Switch: & {\bf port 18}   & -- SPEC running WR PTP Core in \emph{WR Slave} mode\\
    WR Switch: & {\bf 1-PPS in}  & -- 1-PPS signal from external atomic clock\\
    WR Switch: & {\bf 10 MHz in} & -- 10 MHz signal from external atomic clock\\
    WR Switch: & {\bf 1-PPS out} & -- channel 1 of the oscilloscope\\
    ext clock: & {\bf 1-PPS out} & -- channel 2 of the oscilloscope\\
  \end{tabular}
\end{table}

\paragraph{Oscilloscope setup}
\vspace{12pt}
\begin{table}[ht!]
  \begin{tabular}{l l}
    {\bf channel 1} & 200mV/div, offset 800mV, DC 50 Ohm\\
    {\bf channel 2} & setting dependent on external clock device\\
    {\bf persistent mode} & ON\\
  \end{tabular}
\end{table}

\paragraph{Test passed if}
\begin{itemize*}
  \item Oscilloscope in persistent mode shows stable 1-PPS relation between the
    external clock and the WR Switch under test
  \item \emph{lm32-vuart} tool on WR Switch reports through the whole test
    period that device is locked to external clock:\\
    \texttt{Sequencer\_state 8 mode 1 Alignment\_state 7 HL1 EL1 ML0}
  \item \emph{stat} command on slave SPECs reports through the whole test
    period:
    \begin{itemize}
      \item \emph{lock} always \emph{1}
      \item \emph{servo state} always \emph{TRACK\_PHASE}
      \item \emph{mu} changes without any sudden, few-ns jumps
      \item \emph{cko} always in a range -50ps; +50ps
    \end{itemize}
\end{itemize*}

\newpage %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Locking on link up}

The test verifies if the switch under test always locks with sub-nanosecond
offset to another WR Master device when the physical link is established.

\paragraph{Test setup}
\vspace{12pt}
\begin{table}[ht!]
  \begin{tabular}{r l l}
    WR Switch: & {\bf port 0}    & -- SPEC running WR PTP Core in \emph{WR Master} mode\\
  Master SPEC: & {\bf 1-PPS out} & -- channel 1 of the oscilloscope\\
    WR Switch: & {\bf 1-PPS out} & -- channel 2 of the oscilloscope\\
  \end{tabular}
\end{table}

\paragraph{Oscilloscope setup}
\vspace{12pt}
\begin{table}[ht!]
  \begin{tabular}{l l}
    {\bf channel 1} & 200mV/div, offset 1.25V, DC 50Ohm\\
    {\bf channel 2} & 200mV/div, offset 800mV, DC 50Ohm\\
    {\bf persistent mode} & ON\\
  \end{tabular}
\end{table}

\paragraph{Link breaker testing device setup}
\vspace{12pt}
Program infinite loop:\\
  \texttt{Link down}\\
  \texttt{sleep 10s}\\
  \texttt{Link up}\\
  \texttt{sleep 50s}\\

\paragraph{Test passed if}
\begin{itemize*}
  \item Oscilloscope in persistent mode shows stable 1-PPS relation between the
    Master SPEC and the WR Switch under test - i.e. each of the devices produces
    a single, blurred trace on the screen
\end{itemize*}

\newpage %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Synchronization with background traffic}

The test verifies if two WR Switches are able to perform stable, sub-nanosecond
synchronization when a background, non-PTP traffic of various loads is present.

\paragraph{Test setup}
\vspace{12pt}
\begin{table}[ht!]
  \begin{tabular}{r l l}
    WR Switch 1: & {\bf port 17}   & -- Networking tester device\\
    WR Switch 1: & {\bf port 2}    & -- port 0 of WR Switch 2\\
    WR Switch 2: & {\bf port 0}    & -- port 2 of WR Switch 1\\
    WR Switch 2: & {\bf port 17}   & -- Networking tester device\\
    WR Switch 1: & {\bf 1-PPS out} & -- channel 1 of the oscilloscope\\
    WR Switch 2: & {\bf 1-PPS out} & -- channel 2 of the oscilloscope\\
  \end{tabular}
\end{table}

\paragraph{Oscilloscope setup}
\vspace{12pt}
\begin{table}[ht!]
  \begin{tabular}{l l}
    {\bf channel 1, 2} & 200mV/div, offset 800mV, DC 50Ohm\\
       {\bf timescale} & 1ns/div\\
 {\bf persistent mode} & ON\\
  \end{tabular}
\end{table}

\paragraph{Networking tester setup}
\vspace{12pt}
\begin{table}[ht!]
  \begin{tabular}{l l l}
    {\bf Traffic} & send to:      & WR Switch 1, port 17\\
                  & receive on:   & WR Switch 2, port 17\\
     {\bf Frames} & broadcast     & dstMAC = \emph{FF:FF:FF:FF:FF:FF}\\
                  & size [bytes]  & 64, 65, 700,701, 1521, 1522\\
                  & loads [\%]    & 10, 20, 30, 40, 50, 60, 70, 80, 90, 100\\
                  & time of burst per load & 5 min\\
  \end{tabular}
\end{table}

\paragraph{Test passed if}
\begin{itemize*}
  \item For each load of non-PTP frames Oscilloscope in persistent mode shows
    stable 1-PPS relation between two WR Switches\\
  \item \emph{wr\_mon} tool on WR Switch 2 reports through the test period for
    each frames load:
    \begin{itemize}
      \item \emph{lock} always 1
      \item \emph{servo state} always \emph{TRACK\_PHASE}
      \item \emph{mu} changes without any sudden, few-ns jumps
      \item \emph{cko} always in a range -50ps; +50ps
    \end{itemize}
\end{itemize*}

\paragraph{Recorded parameters}
\begin{itemize*}
  \item accuracy (average) and precision (sdev) measured by Oscilloscope measurement of PPS
  \item latency of frames (avg, max, min) and frame loss (\%)
\end{itemize*}


\newpage %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Synchronization with high switch load}

The test verifies if two WR Switches are able to perform stable, sub-nanosecond
synchronization when a background, non-PTP traffic of various loads is present.

\paragraph{Test setup}
\vspace{12pt}
\begin{table}[ht!]
  \begin{tabular}{r l l}
    WR Switch 1: & {\bf port 17}   & -- Networking tester device\\
    WR Switch 1: & {\bf port 2}    & -- port 0 of WR Switch 2\\
    WR Switch 2: & {\bf port 0}    & -- port 2 of WR Switch 1\\
    WR Switch 2: & {\bf port 17}   & -- Networking tester device\\
    WR Switch 1: & {\bf 1-PPS out} & -- channel 1 of the oscilloscope\\
    WR Switch 2: & {\bf 1-PPS out} & -- channel 2 of the oscilloscope\\
  \end{tabular}
\end{table}

\paragraph{Oscilloscope setup}
\vspace{12pt}
\begin{table}[ht!]
  \begin{tabular}{l l}
    {\bf channel 1, 2} & 200mV/div, offset 800mV, DC 50Ohm\\
       {\bf timescale} & 1ns/div\\
 {\bf persistent mode} & ON\\
  \end{tabular}
\end{table}

\paragraph{Networking tester setup}
\vspace{12pt}
\begin{table}[ht!]
  \begin{tabular}{l l l}
    {\bf Traffic} & send to:      & WR Switch 1, port 17\\
                  & receive on:   & WR Switch 2, port 17\\
     {\bf Frames} & broadcast     & dstMAC = \emph{FF:FF:FF:FF:FF:FF}\\
                  & size [bytes]  & 64, 65, 700,701, 1521, 1522\\
                  & loads [\%]    & 10, 20, 30, 40, 50, 60, 70, 80, 90, 100\\
                  & time of burst per load & 5 min\\
  \end{tabular}
\end{table}

\paragraph{Test passed if}
\begin{itemize*}
  \item For each load of non-PTP frames Oscilloscope in persistent mode shows
    stable 1-PPS relation between two WR Switches\\
  \item \emph{wr\_mon} tool on WR Switch 2 reports through the test period for
    each frames load:
    \begin{itemize}
      \item \emph{lock} always 1
      \item \emph{servo state} always \emph{TRACK\_PHASE}
      \item \emph{mu} changes without any sudden, few-ns jumps
      \item \emph{cko} always in a range -50ps; +50ps
    \end{itemize}
\end{itemize*}

